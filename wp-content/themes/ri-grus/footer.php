<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Grus
 * @since Grus 1.0
 */
?>

	</div><!-- .site-content -->
<?php
if(get_post_type()!='product')
echo '</div>';
    ?>
	<!-- .site-container -->
<footer id="footer-<?php if(get_post_meta( get_the_ID(), 'rit_footer_options', true )!='')echo get_post_meta( get_the_ID(), 'rit_footer_options', true ); else echo  get_theme_mod('rit_default_footer', 'default');?>" class="footer-page site-footer">
    <div class="container">

        <?php
        if(get_post_meta( get_the_ID(), 'rit_footer_options', true )!=''){
            get_template_part('included/templates/footer/footer', get_post_meta( get_the_ID(), 'rit_footer_options', true ));
        }
        else{
            get_template_part('included/templates/footer/footer', get_theme_mod('rit_default_footer', 'default'));
        }
         ?>
    </div>
</footer>
<div id="back-to-top"><i class="fa fa-angle-up fa-2x"></i></div>
</div>
</div>
<?php wp_footer(); ?>
<?php
if(get_theme_mod('rit_tracking_code_position')=='footer'){
    echo get_theme_mod('rit_tracking_code_code');
}
?>
</body>
</html>