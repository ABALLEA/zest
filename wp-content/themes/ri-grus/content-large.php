<?php
/**
 * The template displaying content follow large layout
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Grus
 * @since Grus 1.0
 */
?>

<article
    id="post-<?php the_ID(); ?>" <?php (is_single()) ? post_class('single-post news-item') : post_class('post-item'); ?>>
    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <?php
            if (has_post_format('gallery')) : ?>

                <?php $images = get_post_meta($post->ID, '_format_gallery_images', true); ?>

                <?php if ($images) : ?>
                    <div class="post-image<?php echo esc_attr((is_single()) ? ' single-image' : ''); ?>">
                        <ul class="bxslider">
                            <?php foreach ($images as $image) : ?>

                                <?php $the_image = wp_get_attachment_image_src($image, 'full-thumb'); ?>
                                <?php $the_caption = get_post_field('post_excerpt', $image); ?>
                                <li><img src="<?php echo esc_url($the_image[0]); ?>"
                                         <?php if ($the_caption) : ?>title="<?php echo esc_attr($the_caption); ?>"<?php endif; ?> />
                                </li>

                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>

            <?php elseif (has_post_format('video')) : ?>

                <div class="post-image<?php echo (is_single()) ? ' single-video' : ''; ?>">
                    <?php $sp_video = get_post_meta($post->ID, '_format_video_embed', true); ?>
                    <?php if (wp_oembed_get($sp_video)) : ?>
                        <?php echo(wp_oembed_get($sp_video)); ?>
                    <?php else : ?>
                        <?php echo($sp_video); ?>
                    <?php endif; ?>
                </div>

            <?php elseif (has_post_format('audio')) : ?>

                <div class="post-image audio<?php echo (is_single()) ? ' single-audio' : ''; ?>">
                    <?php $sp_audio = get_post_meta($post->ID, '_format_audio_embed', true); ?>
                    <?php if (wp_oembed_get($sp_audio)) : ?>
                        <?php echo(wp_oembed_get($sp_audio)); ?>
                    <?php else : ?>
                        <?php echo($sp_audio); ?>
                    <?php endif; ?>
                </div>

            <?php else : ?>

                <?php if (has_post_thumbnail()) : ?>
                    <?php if (!get_theme_mod('sp_post_thumb')) : ?>
                        <div class="wrapper-img <?php echo esc_attr((is_single()) ? ' single-image' : ''); ?>">
                            <a href="<?php echo get_permalink() ?>"
                               title="<?php the_title(); ?>"><?php the_post_thumbnail('full-thumb'); ?></a>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>

            <?php endif; ?>
        </div>
        <div class="col-xs-12 col-sm-8">
            <div class="news-info">
                <?php if (get_theme_mod('rit_enable_page_heading', '1')) { ?>
                    <?php
                    the_title(sprintf('<h3 class="title-news"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h3>');
                    ?>
                <?php } ?>
                <p class="info-post">
                    <a class="author-link"
                       href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"
                       rel="author">
                        <?php echo(get_the_author()); ?>
                    </a>
                    / <?php echo get_the_date('F jS, Y'); ?>
                    / <?php comments_number('0 Comment', '1 Comment', '% Comments'); ?></p>

                <div class="description">
                    <?php

                    the_excerpt();
                    ?>
                </div>
                <!-- .entry-content -->
            </div>

        </div>

    </div>
</article><!-- #post-## -->
