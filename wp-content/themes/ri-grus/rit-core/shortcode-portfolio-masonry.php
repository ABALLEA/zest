<?php
/**
 * Created by PhpStorm.
 * User: ntk
 * Date: 09/06/2015
 * Time: 09:29
 */
$args = array(
    'post_type' => 'portfolio',
    'posts_per_page' => ($atts['number'] > 0) ? $atts['number'] : get_option('posts_per_page')
);

if ($atts['cat']) {
    $cat = explode(',', $atts['cat']);
    $args['tax_query'] = array(
        array(
            'taxonomy' => 'portfolio_category',
            'field' => 'id',
            'terms' => $cat,
        )
    );
}

if ($atts['post_in'])
    $args['post__in'] = explode(',', $atts['post_in']);
$args['paged'] = (get_query_var('paged')) ? get_query_var('paged') : 1;
$the_query = new WP_Query($args); ?>
<?php
//Begin control nav masonry
if ($atts['cat'] == '')
    $terms = get_terms('portfolio_category', '');
else {
    $terms = array();
    $term_ids = explode(',', $atts['cat']);
    if (count($term_ids) > 0) {
        foreach ($term_ids as $id) {
            $terms[] = get_term($id, 'portfolio_category');
        }
    }
}

if (!empty($terms) && !is_wp_error($terms)):?>
    <div class="wrapper-rit-masonry-filter">
        <div id="mobile-masonry-filter"><span><?php _e('All','ri-grus') ?></span><i class="fa fa-angle-down"></i> </div>
        <ul id="rit-masonry-filter">
            <?php if (count($terms) > 0) : ?>
                <li class="active" data-id="all"><span><?php echo __('All', 'ri-grus') ?></span></li>
                <?php foreach (array_filter($terms) as $term) : ?>
                    <li data-id="<?php echo esc_attr($term->slug) ?>"><span><?php echo esc_html($term->name); ?></span></li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
<?php endif; //End control nav masonry ?>
<div id="wrapper-rit-item-masonry">
    <?php if ($the_query->have_posts()) :
        while ($the_query->have_posts()) : $the_query->the_post();
            //get list catslug
            $catslug = '';
            $termspost = get_the_terms(get_the_ID(), 'portfolio_category');
            if ($termspost && !is_wp_error($termspost)) :
                foreach ($termspost as $term) :
                    $catslug .= $term->slug . ' ';
                endforeach;
            endif;
            ?>
            <div class="rit-item-masonry all <?php echo esc_attr($catslug) ?>">
                <?php echo rit_get_template_part('post-format/portfolio', 'default', array('atts' => $atts)) ?>
                <div class="rit-masonry-mask">
                    <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"></a>

                    <div class="rit-wrapper-mask">
                        <h3 class="rit-title"><a href="<?php the_permalink(); ?>"  title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                        <h4 class="rit-cat"><?php echo get_the_term_list(get_the_ID(), 'portfolio_category', ' ', ' / ', ' '); ?></h4>
                    </div>

                </div>
            </div>
            <?php
        endwhile;
    endif;
    wp_reset_postdata(); ?>
</div><!--End wrapper-rit-item-masonry-->
<script>
    jQuery(window).load(function(){
        ConfigMasonry();
    });
    jQuery(document).ready(function () {
        jQuery('#rit-masonry-filter li').click(function () {
            M(jQuery(this), 0);
        });
        var id;
        jQuery( window ).resize(function(){
            clearTimeout(id);
            id=setTimeout(ConfigMasonry,600);
        });
    });

    function M(Obj, i) {
        i++;
        jQuery('#rit-masonry-filter li.active').removeClass('active');
        Obj.addClass('active');
        var target = Obj.attr('data-id');
        jQuery('#mobile-masonry-filter span').html(target);
        jQuery('div.rit-item-masonry:not(.' + target + ')').addClass('rit_hide_item');
        jQuery('div.' + target).removeClass('rit_hide_item');
        setTimeout(function () {
            jQuery('.rit_hide_item').hide()
        }, 410);
        //RMas(getW('#main>.container'));
        setTimeout(function(){ RMas(getW('#main>.container'));},500);
        setTimeout(function () {
            jQuery('.rit_hide_item').show()
        }, 510);

    }
    function  ConfigMasonry() {
        var w=getW('#main>.container');
        //set width for each item
        jQuery('.rit-item-masonry').each(function(){
            jQuery(this).css({'width':'','transition':'0ms'});
            var c=0;
            c=Math.floor(jQuery(this).outerWidth()/w);
            var w_item = c == 2 ? 2*w: c <= 1  ? w : jQuery('#main>.container').outerWidth();
            jQuery(this).css({'width':w_item,'transition':'500ms'});
        });
        setTimeout(function(){ RMas(w);},600);
    }
    //get width of column
    function getW(t) {
        var columns = jQuery(t).outerWidth() >= 768 ? 3 : jQuery(t).outerWidth() >= 480 ? 2 : 1;
        return jQuery(t).outerWidth() / columns;

    };
    function RMas(w){
        //config for masonry
        var container = document.querySelector('#wrapper-rit-item-masonry');
        var msnry = new Masonry(container, {
            // options
            columnWidth: w,
            itemSelector: '.rit-item-masonry'
        });
    }

</script>
