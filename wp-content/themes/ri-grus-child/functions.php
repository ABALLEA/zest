<?php

/* Styles
=============================================================== */

function ri_child_theme_styles() {
    // Enqueue child theme styles
    wp_enqueue_style( 'ri-child-theme', get_stylesheet_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'ri_child_theme_styles', 1000 ); // Note: Use priority "1000" to include the stylesheet after the parent theme stylesheets
	
add_filter( 'wp_calculate_image_srcset_meta', '__return_null' );
// function register_my_menus() {
//   register_nav_menus(
//     array(
//       'menu-produits' => __( 'Menu Produits Boys & Girls' ),
//       'menu-connexion' => __( 'Menu Connexion & Card' ),
//       'menu-produits-boys' => __( 'Menu Produits Boys' ),
//       'menu-produits-girls' => __( 'Menu Produits Girls' )
//     )
//   );
// }
// add_action( 'init', 'register_my_menus' );

