<header id="sticker" class="header-page relative-header">
    <div class="container">
        <div class="wrapper-content-head clearfix row">
            <div id="logo" class="col-xs-6 col-sm-3">
                <?php get_template_part('included/templates/logo'); ?>
            </div>
            <!-- .site-branding -->
            <nav id="top-nav" class="col-xs-6 col-sm-9">
                <div class="mobile-nav"><span></span></div>
                <div class="wrapper-top-nav">
                    <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_class' => 'nav-menu', 'menu_id' => 'primary-menu')); ?>
                </div>
            </nav>
            
            


            <div id="topsearch-block" class="hidesearch">
                <span id="activesearch"><i class="fa fa-search"> </i></span>
            </div>
            <?php if (class_exists('WooCommerce')) {
                get_template_part('included/templates/topheadcart');
            } ?>
            <span id="searchbtnhead"><i class="fa fa-search"> </i></span>
        </div>
    </div>
</header>
<script>
    jQuery(document).ready(function(){
        jQuery("#sticker").sticky({topSpacing:0});
    });
</script>