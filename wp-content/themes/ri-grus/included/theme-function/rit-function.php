<?php
/**
 * Created by PhpStorm.
 * User: chinhbeo
 * Date: 5/14/15
 * Time: 9:29 AM
 */

/* VARIABLE DEFINITIONS ================================================== */
if (!defined('RIT_TEMPLATE_PATH')) {
    define('RIT_TEMPLATE_PATH', get_template_directory());
}
if (!defined('RIT_INCLUDES_PATH')) {
    define('RIT_INCLUDES_PATH', RIT_TEMPLATE_PATH . '/included');
}
if (!defined('RIT_TEMPLATE_PATH')) {
    define('RIT_LOCAL_PATH', get_template_directory_uri());
}

if (!function_exists('RIT_CUSTOM_CONTENT')) {
    function RIT_CUSTOM_CONTENT()
    {
        include_once(RIT_INCLUDES_PATH . '/rit_header.php');
    }

    add_action('init', 'RIT_CUSTOM_CONTENT', 0);
}

// Shortcode
//include(RIT_INCLUDES_PATH . '/shortcode/shortcode.php');

// Function For RIT Theme
include(RIT_INCLUDES_PATH . '/customize/customize-style.php');

// Include Widgets
include(RIT_INCLUDES_PATH . '/widgets/recent-post.php');
include(RIT_INCLUDES_PATH . '/widgets/search.php');
include(RIT_INCLUDES_PATH . '/widgets/recent-post-sidebar.php');
//VC templates
include(RIT_INCLUDES_PATH . '/vc_templates/grus_about_us.php');
include(RIT_INCLUDES_PATH . '/vc_templates/grus_about_me.php');
include(RIT_INCLUDES_PATH . '/vc_templates/grus_about_minimal.php');
include(RIT_INCLUDES_PATH . '/vc_templates/grus_home.php');
include(RIT_INCLUDES_PATH . '/vc_templates/grus_contact.php');
// Author Link Social
function rit_social_author($contactmethods)
{

    $contactmethods['twitter'] = 'Twitter Username';
    $contactmethods['facebook'] = 'Facebook Username';
    $contactmethods['google'] = 'Google Plus Username';
    $contactmethods['tumblr'] = 'Tumblr Username';
    $contactmethods['instagram'] = 'Instagram Username';
    $contactmethods['pinterest'] = 'Pinterest Username';

    return $contactmethods;
}

add_filter('user_contactmethods', 'rit_social_author', 10, 1);

// Customize
if (class_exists('RIT_Customize')) {
    function rit_customize()
    {
        $rit_customize = RIT_Customize::getInstance();

        $customizers = array(
            'rit_new_section_export_import' => array(
                'title' => __('Export/Import', 'customizer-export-import'),
                'priority' => 2,
                'settings' => array(
                    'rit-setting' => array(
                        'class' => 'cei',
                        'priority' => 1
                    )
                ),
            ),
            'rit_new_section_meta' => array(
                'title' => __('Default Meta Options', 'ri-grus'),
                'description' => '',
                'priority' => 3,
                'settings' => array(
                    'rit_default_blog_layout' => array(
                        'type' => 'select',
                        'label' => __('Default Blog Layout Config','ri-grus'),
                        'description' => '',
                        'priority' => 0,
                        'choices' => array(
                            'default' => __('Default', 'ri-grus'),
                            'large' => __('large', 'ri-grus'),
                        ),
                        'params' => array(
                            'default' => 'default',
                        ),
                    ),
                    'rit_default_sidebar' => array(
                        'type' => 'select',
                        'label' => __('Default Sidebar Config','ri-grus'),
                        'description' => '',
                        'priority' => 0,
                        'choices' => array(
                            'no-sidebar' => __('No Sidebar', 'ri-grus'),
                            'left-sidebar' => __('Left Sidebar', 'ri-grus'),
                            'right-sidebar' => __('Right Sidebar', 'ri-grus'),
                            'both-sidebar' => __('Both Sidebar', 'ri-grus')
                        ),
                        'params' => array(
                            'default' => 'no-sidebar',
                        ),
                    ),
                    'rit_left_sidebar' => array(
                        'type' => 'select',
                        'label' => __('Default Left Sidebar', 'ri-grus'),
                        'description' => '',
                        'priority' => 0,
                        'choices' => rit_sidebar(),
                        'params' => array(
                            'default' => 'sidebar-1',
                        ),
                        'dependence' => array('rit_default_sidebar'=> array('left-sidebar', 'both-sidebar'))
                    ),
                    'rit_right_sidebar' => array(
                        'type' => 'select',
                        'label' => __('Default Right Sidebar', 'ri-grus'),
                        'description' => '',
                        'priority' => 0,
                        'choices' => rit_sidebar(),
                        'params' => array(
                            'default' => 'sidebar-1',
                        ),
                        'dependence' => array('rit_default_sidebar'=> array('right-sidebar', 'both-sidebar'))
                    )
                )
            ),
            'rit_new_section_font_size' => array(
                'title' => __('Font Size Options', 'ri-grus'),
                'description' => '',
                'priority' => 4,
                'settings' => array(
                    'rit_body_font_select' => array(
                        'type' => 'select',
                        'label' => __('Body Font', 'ri-grus'),
                        'description' => '',
                        'priority' => -5,
                        'choices' => array(
                            'standard' => __('Standard', 'ri-grus'),
                            'google' => __('Google', 'ri-grus')
                        ),
                        'params' => array(
                            'default' => 'google',
                        ),
                    ),
                    'rit_body_font_standard' => array(
                        'type' => 'select',
                        'label' => __('Body Standard Font', 'ri-grus'),
                        'description' => '',
                        'priority' => -3,
                        'choices' => array(
                            'Arial' => __('Arial', 'ri-grus'),
                            'Courier New' => __('Courier New', 'ri-grus'),
                            'Georgia' => __('Georgia', 'ri-grus'),
                            'Helvetica' => __('Helvetica', 'ri-grus'),
                            'Lucida Sans' => __('Lucida Sans', 'ri-grus'),
                            'Lucida Sans Unicode' => __('Lucida Sans Unicode', 'ri-grus'),
                            'Myriad Pro' => __('Myriad Pro', 'ri-grus'),
                            'Palatino Linotype' => __('Palatino Linotype', 'ri-grus'),
                            'Tahoma' => __('Tahoma', 'ri-grus'),
                            'Times New Roman' => __('Times New Roman', 'ri-grus'),
                            'Trebuchet MS' => __('Trebuchet MS', 'ri-grus'),
                            'Verdana' => __('Verdana', 'ri-grus')
                        ),
                        'params' => array(
                            'default' => 'Arial',
                        ),
                        'dependence' => array('rit_body_font_select'=> 'standard')
                    ),
                    'rit_body_font_google' => array(
                        'type' => 'select',
                        'label' => __('Body Google Font', 'ri-grus'),
                        'description' => '',
                        'priority' => -4,
                        'choices' => rit_googlefont(),
                        'params' => array(
                            'default' => 'google',
                        ),
                        'dependence' => array('rit_body_font_select'=> 'google')
                    ),
                    'rit_body_font_google_weight' => array(
                        'class' => 'multiple',
                        'label' => __('Body Google Font Weight', 'ri-grus'),
                        'description' => '',
                        'priority' => -2,
                        'choices' => array(
                            '300italic' => 'Light 300 Italic',
                            '400italic' => 'Normal 400 Italic',
                            '600italic' => 'Semi-Bold 600 Italic',
                            '700italic' => 'Bold 700 Italic',
                            '800italic' => 'Extra-Bold 800 Italic',
                            '300' => 'Light 300',
                            '400' => 'Normal 400',
                            '600' => 'Semi-Bold 600',
                            '700' => 'Bold 700',
                            '800' => 'Extra-Bold 800'
                        ),
                        'params' => array(
                            'default' => '',
                        ),
                        'dependence' => array('rit_body_font_select'=> 'google')
                    ),
                    'rit_body_font_google_character' => array(
                        'class' => 'multiple',
                        'label' => __('Body Google Font Character', 'ri-grus'),
                        'description' => '',
                        'priority' => -1,
                        'choices' => array(
                            'cyrillic-ext' => 'Cyrillic Extended (cyrillic-ext)',
                            'greek-ext' => 'Greek Extended (greek-ext)',
                            'greek' => 'Greek (greek)',
                            'vietnamese' => 'Vietnamese (vietnamese)',
                            'latin-ext' => 'Latin Extended (latin-ext)',
                            'cyrillic' => 'Cyrillic (cyrillic)'
                        ),
                        'params' => array(
                            'default' => '',
                        ),
                        'dependence' => array('rit_body_font_select'=> 'google')
                    )
                )
            ),
            //product custom
            'rit_product_custom' => array(
                'title' => __('Custom product display', 'customizer-product-display'),
                'priority' => 5,
                'settings' => array(
                    'rit_product_layout_view' => array(
                        'type' => 'select',
                        'label' => __('Default product Layout', 'ri-grus'),
                        'description' => '',
                        'priority' => 0,
                        'choices' => array(
                            'grid' => __('Grid', 'ri-grus'),
                            'list' => __('List', 'ri-grus')
                        ),
                        'params' => array(
                            'default' => 'Grid',
                        ),
                    ),
                    'rit_number_products_display' => array(
                        'type' => 'number',
                        'label' => __('Default number products display', 'ri-grus'),
                        'description' => '',
                        'priority' => 1,
                        'params' => array(
                            'default' => '9',
                        ),
                    ),
                    'rit_number_products_related_display' => array(
                        'type' => 'number',
                        'label' => __('Default number products related display', 'ri-grus'),
                        'description' => '',
                        'priority' => 2,
                        'params' => array(
                            'default' => '4',
                        ),
                    ),
                    'rit_number_products_related_display_per_row' => array(
                        'type' => 'select',
                        'label' => __('Default number products related display per row', 'ri-grus'),
                        'description' => '',
                        'priority' => 0,
                        'choices' => array(
                            '1' => __('1', 'ri-grus'),
                            '2' => __('2', 'ri-grus'),
                            '3' => __('3', 'ri-grus'),
                            '4' => __('4', 'ri-grus'),
                            '5' => __('5', 'ri-grus'),
                            '6' => __('6', 'ri-grus')
                        ),
                        'params' => array(
                            'default' => '4',
                        ),
                    ),
                    'rit_enable_slider_related_product' => array(
                        'type' => 'select',
                        'label' => __('Enable slider related product', 'ri-grus'),
                        'description' => '',
                        'priority' => 3,
                        'choices' => array(
                            'yes' => __('Yes', 'ri-grus'),
                            'no' => __('No', 'ri-grus')
                        ),
                        'params' => array(
                            'default' => 'no',
                        ),
                    )
                )
            ),
        );
        $rit_customize->add_customize($customizers);
        $rit_customize->rit_register_theme_customizer();
    }

    add_action('customize_register', 'rit_customize');
}

// Substring
if (!function_exists('rit_substring')) {
    function rit_substring($string, $number)
    {
        if (strlen($string) <= $number) {
            return $string;
        } else {
            $new_string = substr($string, 0, $number);
            return $new_string;
        }
    }
}

// Sub String Content
if (!function_exists('rit_content')) {
    function rit_content($limit)
    {
        $content = explode(' ', get_the_content(), $limit);
        if (count($content) >= $limit) {
            array_pop($content);
            $content = implode(" ", $content) . '...';
        } else {
            $content = implode(" ", $content) . '';
        }
        $content = preg_replace('/\[.+\]/', '', $content);
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        return $content;
    }
}

// List Sidebar
if (!function_exists('rit_sidebar')) {
    function rit_sidebar()
    {
        global $wp_registered_sidebars;

        $sidebar_options = array();

        foreach ($wp_registered_sidebars as $sidebar) {
            $sidebar_options[$sidebar['id']] = $sidebar['name'];
        }

        return $sidebar_options;
    }
}

// Category
if (!function_exists('sp_category')) {
    function sp_category($separator)
    {
        $first_time = 1;
        foreach ((get_the_category()) as $category) {
            if ($first_time == 1) {
                echo '<a href="' . esc_src(get_category_link($category->term_id)). '" title="' . sprintf(__("View all posts in %s", "ri-grus"), $category->name) . '" ' . '>' . $category->name . '</a>';
                $first_time = 0;
            } else {
                echo $separator . '<a href="' . esc_src(get_category_link($category->term_id)) . '" title="' . sprintf(__("View all posts in %s", "ri-grus"), $category->name) . '" ' . '>' . $category->name . '</a>';
            }
        }
    }
}


// -------------------- Register Sidebar --------------------- //
if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Sidebar 2',
        'id' => 'sidebar-2',
        'before_widget' => '<div id="%1$s" class="sidebar-item widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="title"><span>',
        'after_title' => '</span></h3>',
    ));
    register_sidebar(array(
        'name' => 'Footer 1',
        'id' => 'footer-1',
        'before_widget' => '<div id="%1$s" class="widget first %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="title">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'name' => 'Footer 2',
        'id' => 'footer-2',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="title">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'name' => 'Footer 3',
        'id' => 'footer-3',
        'before_widget' => '<div id="%1$s" class="widget last %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="title">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'name' => 'Footer 4',
        'id' => 'footer-4',
        'before_widget' => '<div id="%1$s" class="widget last %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="title">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'name' => 'Bottom footer',
        'id' => 'bottom-footer',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="title">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'name' => 'Left product sidebar',
        'id' => 'lp-sidebar',
        'before_widget' => '<aside id="%1$s" class="sidebar-item %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="title"><span>',
        'after_title' => '</span></h3>',
    ));
}

//----Pagination custom for page----//
function posts_nav($totalpages = '')
{
    /*if (is_singular()){
        echo aaaaaaaaaaa;
        return;
    }*/
    if ($totalpages != '') {
        $max = intval($totalpages);
    } else {
        global $wp_query;
        /** Stop execution if there's only 1 page */

        $max = intval($wp_query->max_num_pages);
    }
    if ($max <= 1)
        return;
    $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
    /**    Add current page to the array */
    if ($paged >= 1)
        $links[] = $paged;

    /**    Add the pages around the current page to the array */
    if ($paged >= 3) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if (($paged + 2) <= $max) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="pager"><ul>' . "\n";

    /**    Previous Post Link */
    if (get_previous_posts_link())
        printf('<li>%s</li>' . "\n", get_previous_posts_link('<i class="fa fa-chevron-left"></i>'));

    /**    Link to first page, plus ellipses if necessary */
    if (!in_array(1, $links)) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link(1)), '1');

        if (!in_array(2, $links))
            echo '<li>...</li>';
    }

    /**    Link to current page, plus 2 pages in either direction if necessary */
    sort($links);
    foreach ((array)$links as $link) {
        $class = $paged == $link ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($link)), $link);
    }

    /**    Link to last page, plus ellipses if necessary */
    if (!in_array($max, $links)) {
        if (!in_array($max - 1, $links))
            echo '<li>...</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($max)), $max);
    }

    //var_dump( get_next_posts_link( "Older Entries &rarr;",$max));
    /**    Next Post Link */
    //if (get_next_posts_link($max)){
    printf('<li>%s</li>' . "\n", get_next_posts_link('<i class="fa fa-chevron-right"></i>', $max));
    //}


    echo '</ul></div>' . "\n";

}

//---End pagination custom---//
//----Comments theme custom---//
function mytheme_comment($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);
    if ('div' == $args['style']) {
        $add_below = 'comment';
    } else {
        $add_below = 'div-comment';
    }
    ?>
    <?php if ('li' != $args['style']) : ?>
<li id="div-comment-<?php comment_ID() ?>">
<?php endif;
    echo '<div class="avatar">';
    if ($args['avatar_size'] != 0) echo get_avatar($comment, $args['avatar_size']);
    echo '</div><div class="content-comment">';
    if ($comment->comment_approved == '0') :
        echo '<em class="comment-awaiting-moderation">' . __('Your comment is awaiting moderation.', 'ri-grus') . '</em><br/>';
    endif;
    echo '<div class="info-cmmt clearfix">';
    printf(__('<span class="username">%s</span>'), get_comment_author_link());
    echo '<p>';
    /* translators: 1: date, 2: time */
    printf(__('%1$s at %2$s /', 'ri-grus'), get_comment_date(), get_comment_time());
    comment_reply_link(array_merge($args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'])));
    edit_comment_link(__('/ Edit', 'ri-grus'), '  ', '');
    echo '</p></div>';
    comment_text();
    echo '</div>';
    if ('ul' != $args['style']) :
        echo '</li>';
    endif;
}

//---Add active class for nav--//
add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);
function special_nav_class($classes, $item)
{
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}

//---End add active class---//
//Search form//
function RIT_search_form($form)
{
    $form = '<form role="search" method="get" id="rit_searchform" class="searchform" action="' . home_url('/') . '" >
	<div>
	<input type="text" value="' . get_search_query() . '" name="s" id="s"  placeholder="' . __('Search for:', 'ri-grus') . '" class="ipt text pull-left"/>
	<input type="submit" class="btn btn-submit" id="searchsubmit" value="' . esc_attr__('Search', 'ri-grus') . '" />
	</div>
	</form>';

    return $form;
}

add_filter('get_search_form', 'RIT_search_form');
//End Search form//
