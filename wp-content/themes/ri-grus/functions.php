<?php
//add js, css
//plugin required RIT_LIB::get_posst();
//defined var for template
// update check

/**
 * Grus functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage ri_rus
 * @since Ri Grus 1.0
 */
/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Grus 1.0
 */
if ( ! isset( $content_width ) ) {
    $content_width = 660;
}

/**
 * Grus only works in WordPress 4.1 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.3.1', '<' ) ) {
    require get_template_directory() . '/inc/back-compat.php';
}
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
if ( ! function_exists( 'rit_setup' ) ) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * @since Grus 1.0
     */
    function rit_setup() {

        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Grus, use a find and replace
         * to change 'Grus' to the name of your theme in all the template files
         */
        load_theme_textdomain( 'ri-grus', get_template_directory() . '/languages' );

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support( 'post-thumbnails' );
        set_post_thumbnail_size( 825, 510, true );
        add_theme_support( 'title-tag' );

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus( array(
            'primary' => __( 'Primary Menu',      'ri-grus' ),
            'social'  => __( 'Social Links Menu', 'ri-grus' ),
        ) );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
        ) );

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support( 'post-formats', array(
            'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
        ) );

        /*
         * This theme styles the visual editor to resemble the theme style,
         * specifically font, colors, icons, and column width.
         */
        add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css', rit_fonts_url() ) );
    }
endif; // rit_setup
add_action( 'after_setup_theme', 'rit_setup' );

/**
 * Register widget area.
 *
 * @since Grus 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function rit_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Sidebar 1', 'ri-grus' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Add widgets here to appear in your sidebar.', 'ri-grus' ),
        'before_widget' => '<aside id="%1$s" class="sidebar-item %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="title"><span>',
        'after_title'   => '</span></h3>',
    ) );
}
add_action( 'widgets_init', 'rit_widgets_init' );

if ( ! function_exists( 'rit_fonts_url' ) ) :
    /**
     * Register Google fonts for Grus.
     *
     * @since Grus1.0
     *
     * @return string Google fonts URL for the theme.
     */
    function rit_fonts_url() {
        $fonts_url = '';
        $fonts     = array();
        $subsets   = 'latin,latin-ext';

        /*
         * Translators: If there are characters in your language that are not supported
         * by Noto Sans, translate this to 'off'. Do not translate into your own language.
         */
        if ( 'off' !== _x( 'on', 'Noto Sans font: on or off', 'twentyfifteen' ) ) {
            $fonts[] = 'Noto Sans:400italic,700italic,400,700';
        }

        /*
         * Translators: If there are characters in your language that are not supported
         * by Noto Serif, translate this to 'off'. Do not translate into your own language.
         */
        if ( 'off' !== _x( 'on', 'Noto Serif font: on or off', 'twentyfifteen' ) ) {
            $fonts[] = 'Noto Serif:400italic,700italic,400,700';
        }

        /*
         * Translators: If there are characters in your language that are not supported
         * by Inconsolata, translate this to 'off'. Do not translate into your own language.
         */
        if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'twentyfifteen' ) ) {
            $fonts[] = 'Inconsolata:400,700';
        }

        /*
         * Translators: To add an additional character subset specific to your language,
         * translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
         */
        $subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'twentyfifteen' );

        if ( 'cyrillic' == $subset ) {
            $subsets .= ',cyrillic,cyrillic-ext';
        } elseif ( 'greek' == $subset ) {
            $subsets .= ',greek,greek-ext';
        } elseif ( 'devanagari' == $subset ) {
            $subsets .= ',devanagari';
        } elseif ( 'vietnamese' == $subset ) {
            $subsets .= ',vietnamese';
        }

        if ( $fonts ) {
            $fonts_url = add_query_arg( array(
                'family' => urlencode( implode( '|', $fonts ) ),
                'subset' => urlencode( $subsets ),
            ), '//fonts.googleapis.com/css' );
        }

        return $fonts_url;
    }
endif;

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Grus 1.0
 * */
function rit_javascript_detection() {
    echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'rit_javascript_detection', 0 );

/**
 * Enqueue scripts and styles.
 *
 * @since Grus 1.0
 */
function rit_scripts() {

    // RIT add require
    wp_enqueue_style('RIT_BOOSTRAP', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css');
    wp_enqueue_style('RIT_FONT_AWESOME', get_template_directory_uri() . '/assets/font-awesome/css/font-awesome.min.css');
    wp_enqueue_style('RIT_OWL_CAROUSEL', get_template_directory_uri() . '/assets/owl.carousel/owl.carousel.css');
    wp_enqueue_style('RIT_OWL_THEME', get_template_directory_uri() . '/assets/owl.carousel/owl.theme.css');
    wp_enqueue_style('RIT_BX_CSS', get_template_directory_uri() . '/assets/bxslider/jquery.bxslider.css');
    wp_enqueue_style('RIT_SL_CSS', get_template_directory_uri() . '/assets/selectric/selectric.css');
    wp_enqueue_style('RIT_ELG_CSS', get_template_directory_uri() . '/assets/Elegant-font/style.css');
    wp_enqueue_style('RIT_Custom_Woo_CSS', get_template_directory_uri() . '/css/woocommerce.css');

    // Add custom fonts, used in the main stylesheet.
    wp_enqueue_style( 'rit-fonts', rit_fonts_url(), array(), null );


    // Load our main stylesheet.
    wp_enqueue_style( 'rit-style', get_stylesheet_uri() );

    // Load the Internet Explorer specific stylesheet.
    wp_enqueue_style( 'rit-ie', get_template_directory_uri() . '/css/ie.css', array( 'rit-style' ), '20141010' );
    wp_style_add_data( 'rit-ie', 'conditional', 'lt IE 9' );

    // Load the Internet Explorer 7 specific stylesheet.
    wp_enqueue_style( 'rit-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'rit-style' ), '20141010' );
    wp_style_add_data( 'rit-ie7', 'conditional', 'lt IE 8' );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

    if ( is_singular() && wp_attachment_is_image() ) {
        wp_enqueue_script( 'rit-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20141010' );
    }

    wp_enqueue_script( 'rit-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150330', true );

    wp_localize_script( 'rit-script', 'screenReaderText', array(
        'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'ri-grus' ) . '</span>',
        'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'ri-grus' ) . '</span>',
    ) );

    // RIT JS ICLUDED

    wp_enqueue_script('RIT_JS_CAROUSEL', get_template_directory_uri() . '/assets/owl.carousel/owl.carousel.js');
    wp_enqueue_script('RIT_JS_BX', get_template_directory_uri() . '/assets/bxslider/jquery.bxslider.min.js');
    wp_enqueue_script('RIT_JS_NS', get_template_directory_uri() . '/assets/nicescroll/jquery.nicescroll.min.js');
    wp_enqueue_script('RIT_JS_SL', get_template_directory_uri() . '/assets/selectric/jquery.selectric.min.js');
    wp_enqueue_script('RIT_JS_ST', get_template_directory_uri() . '/js/jquery.sticky.js');
    //wp_enqueue_script('RIT_JS_scrollReveal', get_template_directory_uri() . '/assets/scrollReveal/scrollReveal.min.js');
    if (class_exists('WooCommerce')){
        if (is_woocommerce()) {
        wp_enqueue_script('RIT_JS_CR', get_template_directory_uri() . '/assets/CarouFedsel/jquery.carouFredSel-6.2.1-packed.js');
        wp_enqueue_script('RIT_JS_Zoom', get_template_directory_uri() . '/assets/Zoom/jquery.zoom.min.js');
    }}
    wp_enqueue_script('RIT_JS_THEME', get_template_directory_uri() . '/js/rit.js');
    wp_enqueue_script('RIT_JS_SC', get_template_directory_uri() . '/js/scripts.js');
}
add_action( 'wp_enqueue_scripts', 'rit_scripts' );

/**
 * Add a `screen-reader-text` class to the search form's submit button.
 *
 * @since RIT 1.0
 *
 * @param string $html Search form HTML.
 * @return string Modified search form HTML.
 */
function rit_search_form_modify( $html ) {
    return str_replace( 'class="search-submit"', 'class="search-submit screen-reader-text"', $html );
}
add_filter( 'get_search_form', 'rit_search_form_modify' );

/**
 * Include the TGM_Plugin_Activation class.
 */
require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one included with the TGMPA library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function my_theme_register_required_plugins() {
    /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(

        /* This is an example of how to include a plugin pre-packaged with a theme.
        array(
            'name'               => 'TGM Example Plugin', // The plugin name.
            'slug'               => 'tgm-example-plugin', // The plugin slug (typically the folder name).
            'source'             => get_stylesheet_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
            'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
            'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            'external_url'       => '', // If set, overrides default API URL and points to an external URL.
        ),*/
        //Rit-core
        array(
            'name'      => 'RiverTheme Core',
            'slug'      => 'rit-core', //plugin name
            'source'    => get_stylesheet_directory() . '/lib/plugins/rit-core.zip', // The plugin source.
            'required'  => true,
        ),
        //WPBakery Visual Composer
        array(
            'name'      => 'WPBakery Visual Compose',
            'slug'      => 'js_composer',
            'source'    => get_stylesheet_directory() . '/lib/plugins/js_composer.zip', // The plugin source.
            'required'  => true,
        ),
        //WPBakery Visual Composer
        array(
            'name'      => 'Meta box',
            'slug'      => 'meta-box',
            'source'    => 'https://downloads.wordpress.org/plugin/meta-box.zip', // The plugin source.
            'required'  => true,
        ),
        //woocommerce
        array(
            'name'      => 'WooCommerce',
            'slug'      => 'woocommerce',
            'source'    => 'https://downloads.wordpress.org/plugin/woocommerce.2.4.3.zip',
            'required'  => true,
        ),
        //Contact form 7
        array(
            'name'      => 'Contact Form 7',
            'slug'      => 'contact-form-7',
            'source'    => 'https://downloads.wordpress.org/plugin/contact-form-7.4.2.2.zip',
            'required'  => true,
        ),
        ///Revolution slider
        array(
            'name'      => 'Revolution slider',
            'slug'      => 'revslider',
            'source'    =>  get_stylesheet_directory() . '/lib/plugins/revslider.zip',
            'required'  => true,
        ),
        //Widget import and exporter
        array(
            'name'      => 'Widget import & exporter',
            'slug'      => 'widget-importer-exporter',
            'source'    => 'https://downloads.wordpress.org/plugin/widget-importer-exporter.zip',
            'required'  => true,
        ),
        //Safe report comments
        array(
            'name'      => 'Safe report comments',
            'slug'      => 'safe-report-comments',
            'source'    => 'https://downloads.wordpress.org/plugin/safe-report-comments.0.4.1.zip',
            'required'  => false,
        ),
        //WP User Avatar
        array(
            'name'      => 'WP User Avatar',
            'slug'      => 'wp-user-avatar',
            'source'    => 'https://downloads.wordpress.org/plugin/wp-user-avatar.zip',
            'required'  => false,
        ),
        ///breadcrumb-trail
        array(
            'name'      => 'Breadcrumb trail',
            'slug'      => 'breadcrumb-trail',
            'source'    =>  'https://downloads.wordpress.org/plugin/breadcrumb-trail.1.0.0.zip',
            'required'  => true,
        ),
        //YITH Woocommerce Compare
        array(
            'name'      => 'YITH Woocommerce Compare',
            'slug'      => 'yith-woocommerce-compare',
            'source'    =>  'https://downloads.wordpress.org/plugin/yith-woocommerce-compare.1.2.3.zip',
            'required'  => false,
        ),
        //YITH WooCommerce Wishlist
        array(
            'name'      => 'YITH Woocommerce Wishlist',
            'slug'      => 'yith-woocommerce-wishlist',
            'source'    =>  'https://downloads.wordpress.org/plugin/yith-woocommerce-wishlist.2.0.8.zip',
            'required'  => false,
        ),
        //YITH WooCommerce Quickview
        array(
            'name'      => 'YITH Woocommerce Quick View',
            'slug'      => 'yith-woocommerce-quick-view',
            'source'    =>  'https://downloads.wordpress.org/plugin/yith-woocommerce-quick-view.1.0.4.zip',
            'required'  => false,
        ),
    );

    /*
     * Array of configuration settings. Amend each line as needed.
     *
     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
     * strings available, please help us make TGMPA even better by giving us access to these translations or by
     * sending in a pull-request with .po file(s) with the translations.
     *
     * Only uncomment the strings in the config array if you want to customize the strings.
     *
     * Some of the strings are wrapped in a sprintf(), so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug'  => 'themes.php',            // Parent menu slug.
        'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => __( 'Install Required Plugins', 'theme-slug' ),
            'menu_title'                      => __( 'Install Plugins', 'theme-slug' ),
            'installing'                      => __( 'Installing Plugin: %s', 'theme-slug' ), // %s = plugin name.
            'oops'                            => __( 'Something went wrong with the plugin API.', 'theme-slug' ),
            'notice_can_install_required'     => _n_noop(
                'This theme requires the following plugin: %1$s.',
                'This theme requires the following plugins: %1$s.',
                'theme-slug'
            ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop(
                'This theme recommends the following plugin: %1$s.',
                'This theme recommends the following plugins: %1$s.',
                'theme-slug'
            ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop(
                'Sorry, but you do not have the correct permissions to install the %1$s plugin.',
                'Sorry, but you do not have the correct permissions to install the %1$s plugins.',
                'theme-slug'
            ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop(
                'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
                'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
                'theme-slug'
            ), // %1$s = plugin name(s).
            'notice_ask_to_update_maybe'      => _n_noop(
                'There is an update available for: %1$s.',
                'There are updates available for the following plugins: %1$s.',
                'theme-slug'
            ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop(
                'Sorry, but you do not have the correct permissions to update the %1$s plugin.',
                'Sorry, but you do not have the correct permissions to update the %1$s plugins.',
                'theme-slug'
            ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop(
                'The following required plugin is currently inactive: %1$s.',
                'The following required plugins are currently inactive: %1$s.',
                'theme-slug'
            ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop(
                'The following recommended plugin is currently inactive: %1$s.',
                'The following recommended plugins are currently inactive: %1$s.',
                'theme-slug'
            ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop(
                'Sorry, but you do not have the correct permissions to activate the %1$s plugin.',
                'Sorry, but you do not have the correct permissions to activate the %1$s plugins.',
                'theme-slug'
            ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop(
                'Begin installing plugin',
                'Begin installing plugins',
                'theme-slug'
            ),
            'update_link' 					  => _n_noop(
                'Begin updating plugin',
                'Begin updating plugins',
                'theme-slug'
            ),
            'activate_link'                   => _n_noop(
                'Begin activating plugin',
                'Begin activating plugins',
                'theme-slug'
            ),
            'return'                          => __( 'Return to Required Plugins Installer', 'theme-slug' ),
            'plugin_activated'                => __( 'Plugin activated successfully.', 'theme-slug' ),
            'activated_successfully'          => __( 'The following plugin was activated successfully:', 'theme-slug' ),
            'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'theme-slug' ),  // %1$s = plugin name(s).
            'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'theme-slug' ),  // %1$s = plugin name(s).
            'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'theme-slug' ), // %s = dashboard link.
            'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'tgmpa' ),

            'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );

    tgmpa( $plugins, $config );

}

// Function For RIT Theme
include('included/theme-function/rit-function.php');

//Custom Functions of woo
include('woocommerce/woo-functions.php');

add_action( 'comment_form_logged_in_before', 'additional_fields' );
add_action( 'comment_form_after_fields', 'additional_fields' );

function additional_fields () {
    echo '<p>'.
        '<input id="subject" class="ipt text" name="subject" type="text" size="30" placeholder="Subject"  tabindex="5" /><i class="fa fa-comments"></i></p>';
}