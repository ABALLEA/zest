<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Grus
 * @since Grus 1.0
 */

get_header();
global $post;

$sidebar = $class_main = $class_content = $class_sidebar = '';
if (get_post_meta($post->ID, 'rit_sidebar_options', true) == 'use-default') {
    $sidebar = get_theme_mod('rit_default_sidebar', 'right-sidebar');
} else {
    $sidebar = get_post_meta($post->ID, 'rit_sidebar_options', true);
}
$class_content = $sidebar;

if($sidebar == 'no-sidebar' || $sidebar =='') {
    $class_main = 'col-sm-12 col-md-12';
} elseif ($sidebar == 'left-sidebar' || $sidebar == 'right-sidebar') {
    $class_main = 'col-sm-12 col-md-9 ';
} else {
    $class_main = 'col-sm-12 col-md-6 ';
}
?>
<div id="primary" class="content-area row <?php echo esc_attr($class_content); ?>">
    <?php if ($sidebar == 'left-sidebar' || $sidebar == 'both-sidebar') { ?>
        <!--begin sidebar-->
        <div id="sidebar-left" class="col-sm-12 col-md-3">
            <?php
            $sidebar_left = '';
            if(get_post_meta($post->ID, 'rit_sidebar_options', true) == 'use-default'){

                $sidebar_left = get_theme_mod('rit_default_left_sidebar', 'sidebar-1');
            } else {
                $sidebar_left = get_post_meta($post->ID, 'rit_left_sidebar', true);
            }
            dynamic_sidebar($sidebar_left); ?>

            <!--End left sidebar-->
        </div>
    <?php } ?>
    <main class="site-main <?php echo esc_attr($class_main); ?>">

        <?php
        // Start the loop.
        while (have_posts()) : the_post();

            // Include the page content template.
            the_content();

            // If comments are open or we have at least one comment, load up the comment template.
            //if (is_single()):
                if (comments_open() || get_comments_number()) :
                    comments_template();
                endif;
            //endif;

            // End the loop.
        endwhile;
        ?>

    </main>
    <!-- .site-main -->
    <?php if ($sidebar == 'right-sidebar' || $sidebar == 'both-sidebar') { ?>
        <div id="sidebar-right" class="col-sm-12 col-md-3">
            <?php $sidebar_right = '';
            if(get_post_meta($post->ID, 'rit_sidebar_options', true) == 'use-default'){

                $sidebar_right = get_theme_mod('rit_default_right_sidebar', 'sidebar-1');
            } else {
                $sidebar_right = get_post_meta($post->ID, 'rit_right_sidebar', true);
            }
            dynamic_sidebar($sidebar_right); ?>
        </div>
    <?php } ?>
</div><!-- .content-area -->

<?php get_footer(); ?>
