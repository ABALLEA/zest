<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Grus
 * @since Grus Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php if (get_theme_mod('rit_enable_responsive', '1')) { ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php } ?>
    <?php
    if (get_theme_mod('rit_favicon') != '') {
        echo '<link type="image/x-icon" href="' . get_theme_mod('rit_favicon') . '" rel="shortcut icon">';
    }
    ?>
    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
    <script>(function () {
            document.documentElement.className = 'js'
        })();</script>
    <?php wp_head(); ?>
    <!----Tracking code--->
    <?php
    if (get_theme_mod('rit_tracking_code_position') == 'header') {
        echo get_theme_mod('rit_tracking_code_code');
    }
    ?>
</head>
<?php
$bodyclass = ' page';
if (!is_front_page()) {
    $bodyclass .= ' home-page';
}
if (get_post_meta(get_the_ID(), 'rit_canvas_menu_options', true) == '' || get_post_meta(get_the_ID(), 'rit_canvas_menu_options', true) == 'canvas-default') {
    $bodyclass .= ' ' . get_theme_mod('rit_enable_canvas_menu', 'disable-canvas');
} else {
    $bodyclass .= ' ' . get_post_meta(get_the_ID(), 'rit_canvas_menu_options', true);
}
?>

<body <?php if (get_post_type() != 'product') {
    body_class($bodyclass);
} else echo 'class="page"'; ?>>
<div id="global-search" class="hidesearch">
    <form role="search" method="get" id="searchform" action="<?php echo home_url(); ?>">
        <span id="closesearch"></span>
        <input type="text"
               value="<?php echo esc_attr(apply_filters('the_search_query', get_search_query())); ?>"
               placeholder="<?php _e('Rechercher ici...', 'ri-grus') ?>" class="ipt text" name="s"
               id="s"/>
    </form>
</div>

<div class="global-wrapper">
    <div class="wrapper-page clearfix">
       
       
<header class="header-page fix-header">
    <div class="container">
        <div class="wrapper-content-head clearfix row">
            <div class="inlineLink">
               <a class="inline" href="https://www.instagram.com/zest.toulouse/" class="afternotvisible"><img src="http://zestore.fr/inc/uploads/2016/01/insat50.png"></a>
                <a id="facebookIcon" class="inline" href="https://www.facebook.com/zesttoulouse/?fref=ts" class="afternotvisible"><img src="http://zestore.fr/inc/uploads/2016/01/FB50.png"></a>
                
<!--                <?php get_template_part('included/templates/logo'); ?>-->
            </div>
            <!-- .site-branding -->
            <nav class="middleLogo">
                <a href="<?php echo get_home_url(); ?>" class="LogoLink">
                   
                    <img class="inline" src="<?php echo get_home_url(); ?>/inc/uploads/2016/01/Logo-Zest.svg">
                    <img id="citron" class="inline" src="<?php echo get_home_url(); ?>/inc/uploads/2016/01/Citron.gif">
                    <img class="inline" src="<?php echo get_home_url(); ?>/inc/uploads/2016/01/Logo-Zest.svg">
                
                </a>
            </nav>
            
  

            <div id="topsearch-block" class="hidesearch">
                <span id="activesearch"><i class="fa fa-search"> </i></span>
            </div>
            <?php if (class_exists('WooCommerce')) {
                get_template_part('included/templates/topheadcart');
            } ?>
            <span id="searchbtnhead"><i class="fa fa-search"> </i></span>
        </div>
    </div>
    
</header>
<div class="contentPusher"></div>
       
        <?php
        if (get_post_meta(get_the_ID(), 'rit_slider_shortcode', true) != ''):
            echo do_shortcode(get_post_meta(get_the_ID(), 'rit_slider_shortcode', true));
            echo '<div id="main" class="clearfix" style="margin-top:30px">';
        else:
        ?>
        <div id="main"
             class="clearfix header-<?php if (get_post_meta(get_the_ID(), 'rit_header_options', true) == 'use-default' || get_post_meta(get_the_ID(), 'rit_header_options', true) == '') {
                 echo get_theme_mod('rit_default_header', '1');
             } else echo get_post_meta(get_the_ID(), 'rit_header_options', true); ?>">
            <?php if (get_post_meta(get_the_ID(), 'rit_header_options', true) == 'use-default' && get_theme_mod('rit_default_header', '1') == '1') { ?>
                <script>
                    jQuery(document).ready(function () {
                        if (parseInt(jQuery('#main').css('margin-top')) < jQuery('header.header-page').outerHeight()) {
                            jQuery('#main').css('margin-top', jQuery('header.header-page').outerHeight());
                        }
                    })
                </script>
            <?php
            }
            elseif (get_post_meta(get_the_ID(), 'rit_header_options', true) == '1'){
            ?>
                <script>
                    jQuery(document).ready(function () {
                        if (parseInt(jQuery('#main').css('margin-top')) < jQuery('header.header-page').outerHeight()) {
                            jQuery('#main').css('margin-top', jQuery('header.header-page').outerHeight());
                        }
                    })
                </script>
                <?php
            }
            endif;
            ?>
            <div class="freeShipping">
                <div class="container">
                    <span>FREE SHIPPING 100€</span>
                </div>
            </div> 
<!--
            <a href="https://www.instagram.com/zest.toulouse/" class="afternotvisible"><img src="http://zestore.fr/inc/uploads/2016/01/insat50.png"></a>
            <a href="https://www.facebook.com/zesttoulouse/?fref=ts" class="afternotvisible"><img src="http://zestore.fr/inc/uploads/2016/01/FB50.png"></a>
-->
            <?php if (!is_front_page() && 'portfolio' != get_post_type() && get_post_type() != 'product' && get_page_template_slug() != 'template-portfolio-page.php' && get_page_template_slug() != 'template_no_breadcrumb.php' && get_page_template_slug() != 'template_home.php') {
                ?>
                <div class="wrapper-breadcrumb breadcrumb-portfolio-page">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 pull-left text-left">
                                <h3 class="page-title">
                                    <?php
                                    if (is_single() || is_page()) {
                                        if (get_post_meta(get_the_ID(), 'rit_disible_title', true) == '0') {
                                            the_title();
                                        }
                                    } else {
                                        if (is_archive()) {
                                            echo single_month_title(' ');
                                        }
                                        if (is_category()) {
                                            echo single_cat_title('', false);
                                        }
                                    }
                                    ?>
                                </h3>
                            </div>
                            <div class="col-xs-12 col-sm-6 pull-right text-right">
                                <?php if (function_exists('breadcrumb_trail')) breadcrumb_trail(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php
            }
            ?>
<?php
if (get_post_type() != 'product')
    echo '<div class="container">';
?>
                
