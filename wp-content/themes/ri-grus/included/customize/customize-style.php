<?php
//////////////////////////////////////////////////////////////////
// Customizer - Add CSS
//////////////////////////////////////////////////////////////////

function rit_customizer_css()
{
    global $post;
    ?>
    <style type="text/css">

        <?php
             $font_family = (get_theme_mod('rit_body_font_select', 'google') == 'google') ? get_theme_mod('rit_body_font_google', 'Roboto') : get_theme_mod('rit_body_font_standard', 'Arial');
             $font_weight = get_theme_mod('rit_body_font_google_weight', array('400'));

             if(get_theme_mod('rit_body_font_select', 'google')) {

             $google_font = '//fonts.googleapis.com/css?family=';
             $google_font .= str_replace(' ', '+', $font_family) . ':';

             $google_font .= implode(',', $font_weight);
         ?>
        @import url(<?php echo $google_font; ?>);
        <?php } ?>
        <?php // ---------  Body ------------ // ?>
        *, .item-related h3 a, .title-news a, .title-news, #rit-masonry-filter li, .wrapper-related-portfolio .title, .wrapper-related-portfolio .title-news a,
        .rit-news-item .title-news, .cart_totals .title, .compare-list
        {
            font-family: "<?php echo $font_family; ?>";
        }
		body{
			font-family: "<?php echo $font_family; ?>";
            font-size: <?php echo get_theme_mod('rit_enable_body_font_size', '14'); ?>px;
            line-height: <?php echo get_theme_mod('rit_enable_bodyline_height', '20'); ?>px;
		}
		.widget_categories ul li a, .widget_archive ul li a, .widget_meta a{
			font-size: <?php echo get_theme_mod('rit_enable_body_font_size', '14'); ?>px;
		}
		.wrapper-top-nav .nav-menu a, #top-nav div > ul > li > a, #top-nav .sub-menu li a{
			font-size:<?php echo get_theme_mod('rit_enable_menu_font_size', '13'); ?>px;
		}
        body {
            background-color: <?php echo get_theme_mod('rit_body_bg_color', '#ffffff'); ?>;
        <?php if(get_post_meta($post->ID, 'rit_inner_background_image', true)!=''):?>
            background-image: url(<?php echo wp_get_attachment_url(get_post_meta($post->ID, 'rit_inner_background_image', true)); ?>);
        <?php
        else:
         if(get_theme_mod('rit_body_bg_image', '')){ ?> background-image: url(<?php echo get_theme_mod('rit_body_bg_image', ''); ?>);
        <?php } ?><?php
           endif;
            ?>
        }
        <?php // ---------  Accent Color ------------ // ?>
        .accent, .accent-color {
            color: <?php echo get_theme_mod('rit_accent_color', '#282828'); ?>;
        }

        a {
            color: <?php echo get_theme_mod('rit_body_link_color', '#282828'); ?>;
        }

        a:hover {
            color: <?php echo get_theme_mod('rit_body_link_hover_color', '#282828'); ?>;
        }

        h1 {
            color: <?php echo get_theme_mod('rit_body_h1_color', '#282828'); ?>;
        }

        h2 {
            color: <?php echo get_theme_mod('rit_body_h2_color', '#282828'); ?>;
        }

        h3 {
            color: <?php echo get_theme_mod('rit_body_h3_color', '#282828'); ?>;
        }

        h4 {
            color: <?php echo get_theme_mod('rit_body_h4_color', '#282828'); ?>;
        }

        h5 {
            color: <?php echo get_theme_mod('rit_body_h5_color', '#282828'); ?>;
        }

        h6 {
            color: <?php echo get_theme_mod('rit_body_h6_color', '#282828'); ?>;
        }

        <?php // ---------  Header ------------ // ?>
        .visible-header, .relative-header {
            background-color: <?php echo get_theme_mod('rit_header_background_color', '#fff'); ?>;
            color: <?php echo get_theme_mod('rit_header_text_color', '#282828'); ?>;
        }

        .header-page a {
            color: <?php echo get_theme_mod('rit_header_link_color', '#282828'); ?>;
        }

        .header-page a:hover {
            color: <?php echo get_theme_mod('rit_header_link_hover_color', '#282828'); ?>;
        }

        <?php // ---------  Main Navigation ------------ // ?>

        .relative-header #top-nav {
            background: <?php echo get_theme_mod('rit_nav_bg_color', '#fff'); ?>
        }

        #top-nav #primary-menu > li {
            color: <?php echo get_theme_mod('rit_nav_text_color', '#000'); ?>
        }

        #top-nav #primary-menu > li > a {
            color: <?php echo get_theme_mod('rit_nav_link_color', '#000'); ?>
        }

        #top-nav #primary-menu > li > a:hover {
            color: <?php echo get_theme_mod('rit_nav_link_hover_color', '#000'); ?>
        }

        #top-nav #primary-menu > li > a:hover:after {
            background-color: <?php echo get_theme_mod('rit_nav_link_hover_color', '#000'); ?>
        }

        #top-nav #primary-menu .sub-menu {
            background-color: <?php echo get_theme_mod('rit_nav_sub_bg_color', '#fff'); ?>
        }

        #top-nav #primary-menu .sub-menu li a {
            color: <?php echo get_theme_mod('rit_nav_sub_link_color', '#282828'); ?>
        }

        #top-nav #primary-menu .sub-menu li a:hover {
            color: <?php echo get_theme_mod('rit_nav_sub_link_hover_color', '#000'); ?>
        }

        <?php // ---------  Logo ------------ // ?>
        #logo {
            padding-top: <?php echo get_theme_mod('rit_logo_top_spacing', '0') ?>;
            padding-bottom: <?php echo get_theme_mod('rit_logo_bottom_spacing', '0') ?>;
        }

        #logo img {
            height: <?php echo get_theme_mod('rit_logo_height', 'auto') ?>px;
        }

        <?php // ---------  Footer ------------ // ?>
        .site-footer {
            background-color: <?php echo get_theme_mod('rit_footer_background_color', 'transparent'); ?>;
            color: <?php echo get_theme_mod('rit_footer_text_color', '#282828'); ?>;
        }

        .site-footer a {
            color: <?php echo get_theme_mod('rit_footer_link_color', '#282828'); ?>;
        }

        .site-footer a:hover {
            color: <?php echo get_theme_mod('rit_footer_link_hover_color', '#282828'); ?>;
        }

        #bottom-footer {
            border-top: <?php echo get_theme_mod('rit_border_footer_height', '5'); ?>px solid <?php echo get_theme_mod('rit_footer_border_color', '#f7f7f7'); ?>
        }

        <?php // ---------  Coppy Right ------------ // ?>
        #copy-right {
            background-color: <?php echo get_theme_mod('rit_copyright_bg_color', 'transparent'); ?>;
            color: <?php echo get_theme_mod('rit_copyright_text_color', '#282828'); ?>;
        }

        #copy-right a {
            color: <?php echo get_theme_mod('rit_copyright_link_color', '#282828'); ?>;
        }

        #copy-right a:hover {
            color: <?php echo get_theme_mod('rit_copyright_link_hover_color', '#282828'); ?>;
        }

        <?php // ---------  Custom Style ------------ // ?>
        <?php if(get_theme_mod( 'rit_custom_css' )) : ?>
        <?php echo get_theme_mod( 'rit_custom_css' ); ?>
        <?php endif; ?>

    </style>
    <?php
}

add_action('wp_head', 'rit_customizer_css');

?>