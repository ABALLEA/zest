<?php if ( is_front_page() && is_home() ) { ?>
    <?php if(!get_theme_mod('rit_logo')) { ?>
        <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
    <?php } else { ?>
        <h1 class="site-logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"  title="<?php bloginfo( 'name' ); ?>"><img src="<?php echo esc_url(get_theme_mod('rit_logo')) ?>" class="img-responsive" alt="<?php bloginfo( 'name' ); ?>" /></a></h1>
        <?php if(get_theme_mod('rit_logo_retina')) { ?>
            <h1 class="site-logo" id="logo-retina"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"  title="<?php bloginfo( 'name' ); ?>"><img src="<?php echo esc_url(get_theme_mod('rit_logo_retina')) ?>" class="img-responsive" alt="<?php bloginfo( 'name' ); ?>" /></a></h1>
        <?php } ?>
    <?php }} else { ?>
    <?php if(!get_theme_mod('rit_logo')) { ?>
         <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"  title="<?php bloginfo( 'name' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
    <?php } else { ?>
        <p class="site-logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"  title="<?php bloginfo( 'name' ); ?>"><img src="<?php echo esc_url(get_theme_mod('rit_logo')) ?>" class="img-responsive" alt="<?php bloginfo( 'name' ); ?>" /></a></p>
        <?php if(get_theme_mod('rit_logo_retina')) { ?>
            <p class="site-logo" id="logo-retina"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"  title="<?php bloginfo( 'name' ); ?>"><img src="<?php echo esc_url(get_theme_mod('rit_logo_retina')) ?>"  class="img-responsive" alt="<?php bloginfo( 'name' ); ?>" /></a></p>
        <?php } ?>
    <?php }} ?>
<?php
if(get_theme_mod('rit_show_tagline')==1):
$description = get_bloginfo( 'description', 'display' );
if ( $description || is_customize_preview() ) { ?>
    <p class="site-description"><?php echo $description; ?></p>
<?php }
    endif;
    ?>