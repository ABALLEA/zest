<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Grus
 * @since Grus 1.0
 */
?>
<div class="content-page">
<?php the_content(); ?>
</div>
		<?php
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'ri-grus' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'ri-grus' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>
	<?php edit_post_link( __( 'Edit', 'ri-grus' ), '<footer class="entry-footer"><span class="edit-link"><i class="fa fa-edit"></i> ', '</span></footer><!-- .entry-footer -->' ); ?>

