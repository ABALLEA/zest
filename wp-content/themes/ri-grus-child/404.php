<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Grus
 * @since Grus 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main class="site-main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1>
						404
					</h1>
				</header><!-- .page-header -->
				<h2 class="page-title"><?php _e( 'Oops! La page que vous recherchez est introuvable', 'ri-grus' ); ?></h2>
				<p><a href="<?php echo get_home_url(); ?>" class="back-home" title="<?php _e( 'back to homepage', 'ri-grus' ); ?>"> <?php _e( 'Retour accueil', 'ri-grus' ); ?></a></p>
			</section><!-- .error-404 -->

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
