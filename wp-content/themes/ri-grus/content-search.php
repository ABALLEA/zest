<?php
/**
 * The template part for displaying results in search pages
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Grus
 * @since Grus 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post-item search-result-item'); ?>>

    <?php if (has_post_format('gallery')) : ?>

        <?php $images = get_post_meta($post->ID, '_format_gallery_images', true); ?>

        <?php if ($images) : ?>
            <div class="post-image<?php echo (is_single()) ? ' single-image' : ''; ?>">
                <ul class="bxslider">
                    <?php foreach ($images as $image) : ?>

                        <?php $the_image = wp_get_attachment_image_src($image, 'full-thumb'); ?>
                        <?php $the_caption = get_post_field('post_excerpt', $image); ?>
                        <li><img src="<?php echo esc_url($the_image[0]); ?>"
                                 <?php if ($the_caption) : ?>title="<?php echo esc_attr($the_caption); ?>"<?php endif; ?> /></li>

                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>

    <?php elseif (has_post_format('video')) : ?>

        <div class="post-image<?php echo (is_single()) ? ' single-video' : ''; ?>">
            <?php $sp_video = get_post_meta($post->ID, '_format_video_embed', true); ?>
            <?php if (wp_oembed_get($sp_video)) : ?>
                <?php echo esc_html(wp_oembed_get($sp_video)); ?>
            <?php else : ?>
                <?php echo esc_html($sp_video); ?>
            <?php endif; ?>
        </div>

    <?php elseif (has_post_format('audio')) : ?>

        <div class="post-image audio<?php echo (is_single()) ? ' single-audio' : ''; ?>">
            <?php $sp_audio = get_post_meta($post->ID, '_format_audio_embed', true); ?>
            <?php if (wp_oembed_get($sp_audio)) : ?>
                <?php echo esc_html(wp_oembed_get($sp_audio)); ?>
            <?php else : ?>
                <?php echo esc_html($sp_audio); ?>
            <?php endif; ?>
        </div>

    <?php else : ?>

        <?php if (has_post_thumbnail()) : ?>
            <?php if (!get_theme_mod('sp_post_thumb')) : ?>
                <div class="wrapper-img <?php echo (is_single()) ? ' single-image' : ''; ?>">
                    <a href="<?php echo get_permalink() ?>"
                       title="<?php the_title(); ?>"><?php the_post_thumbnail('full-thumb'); ?></a>

                    <div class="wrapper-mask">
                        <div class="mask" data-link="<?php the_permalink(); ?>">
							<span class="readmore">
								<a href="<?php echo get_permalink() ?>"
                                   title="<?php echo __('Read more', 'ri-grus') ?>"><?php echo __('Read more', 'ri-grus') ?></a>
							</span>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endif; ?>

    <?php endif; ?>
    <div class="news-info">
        <?php if (get_theme_mod('rit_enable_page_heading', '1')) {
            the_title(sprintf('<h3 class="title-news"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h3>');
        } ?>
        <p class="info-post">
            <a class="author-link" href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"
               rel="author">
                <?php echo(get_the_author()); ?>
            </a>
            / <?php echo get_the_date('F jS, Y'); ?>
            / <?php comments_number('0 Comment', '1 Comment', '% Comments'); ?></p>

        <div class="description">
            <?php
            the_excerpt();
            ?>
        </div>
        <!-- .entry-content -->
    </div>
</article><!-- #post-## -->

