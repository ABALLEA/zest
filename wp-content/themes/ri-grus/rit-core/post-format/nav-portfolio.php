<?php
/**
 * Created by PhpStorm.
 * User: NTK
 * Date: 14/07/2015
 * Time: 3:04 CH
 */
//Begin control nav masonry
if($atts['cat'] == '')
    $terms = get_terms('portfolio_category', '');
else {
    $terms = array();
    $term_ids = explode(',', $atts['cat']);
    if(count($term_ids) > 0) {
        foreach($term_ids as $id) {
            $terms[] = get_term($id, 'portfolio_category');
        }
    }
}
if (!empty($terms) && !is_wp_error($terms)):?>
    <div class="wrapper-rit-masonry-filter">
        <div id="mobile-masonry-filter"><span><?php _e('All','ri-grus')?></span><i class="fa fa-angle-down"></i> </div>
        <ul id="rit-masonry-filter">
            <?php if (count($terms) > 0) : ?>
                <li class="active" data-id="all"><span><?php echo __('All', 'ri-grus') ?></span></li>
                <?php foreach (array_filter($terms) as $term) : ?>
                    <li data-id="<?php echo esc_attr($term->slug) ?>"><span><?php echo esc_html($term->name); ?></span></li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
    <script>
        jQuery(window).load(function(){
            setTimeout(function () {
                ConfigMasonry();
            }, 50);
            setTimeout(function () {
                ConfigMasonry();
            }, 100);
        })
        jQuery(document).ready(function () {

            jQuery('#rit-masonry-filter li').click(function () {
                M(jQuery(this),0);
            });
            var id;
            jQuery( window ).resize(function(){
                clearTimeout(id);
                id=setTimeout(ConfigMasonry,800);
            });
        });
        function ConfigMasonry() {
            var container = document.querySelector('#wrapper-rit-item-masonry');
            var msnry = new Masonry(container, {
                // options
                columnWidth: jQuery('.rit-news-item').outerWidth(),
                itemSelector: '.rit-item-masonry'
            });
        }
        function M(Obj,i){
            i++;
            jQuery('#rit-masonry-filter li.active').removeClass('active');
            Obj.addClass('active');
            var target = Obj.attr('data-id');
            jQuery('#mobile-masonry-filter span').html(target);
            jQuery('article.rit-item-masonry:not(.' + target + ')').addClass('rit_hide_item');
            setTimeout(function () {
                jQuery('.rit_hide_item').hide()
            }, 100);
            setTimeout(function () {
                jQuery('.rit_hide_item').show();
            }, 300);
            jQuery('article.' + target).removeClass('rit_hide_item');
            setTimeout(function () {
                ConfigMasonry();
            }, 100);
            if(i<=2){
                setTimeout(function () {
                    M(Obj,i);
                }, 120);
            }
        }
    </script>
<?php endif; //End control nav masonry ?>