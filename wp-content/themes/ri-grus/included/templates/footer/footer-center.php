<div id="bottom-footer" class="clearfix footer-center">
        <?php get_template_part('included/templates/socail'); dynamic_sidebar('bottom-footer'); ?>

        <?php if (get_theme_mod('rit_enable_copyright', '1')) { ?>
            <div class="copy-right" id="copy-right">
                <?php if (get_theme_mod('rit_copyright_text')) {
                    echo get_theme_mod('rit_copyright_text');
                }?>
            </div><!-- .site-info -->
        <?php } ?>
</div>