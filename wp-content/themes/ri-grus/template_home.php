<?php
/**
 * Created by PhpStorm.
 * User: ntk
 * Date: 11/06/2015
 * Time: 15:48
 * Template Name:Home page
 */
get_header();
if ( have_posts() ) :
    // Start the loop.
    while ( have_posts() ) : the_post();
        the_content();
    endwhile; endif;
get_footer();