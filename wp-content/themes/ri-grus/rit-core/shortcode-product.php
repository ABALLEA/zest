<?php

$owlCarousel = '';
$class = ''; ?>
    <div class="wrapper-products-shortcode"  style="padding-bottom:<?php echo esc_attr($atts['padding_bottom_module']) ?>">
        <?php
        if ($atts['products_type'] == 'products_list') {
            $class .= 'list-layout';
        } else {
            $class .= 'grid-layout';
        }
        if ($atts['products_type'] == 'products_carousel') {
            add_action('wp_footer', 'products_carousel_script');
            $owlCarousel = 'owlCarousel';
            $class .= ' products-carousel-list';
            $item;
            switch ($atts['column']) {
                case 'columns1':
                    $item = 1;
                    break;
                case 'columns2':
                    $item = 2;
                    break;
                case 'columns3':
                    $item = 3;
                    break;
                case 'columns4':
                    $item = 4;
                    break;
                case 'columns5':
                    $item = 5;
                    break;
                case 'columns6':
                    $item = 6;
                    break;
                default:
                    $item = 4;
                    break;
            }
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery(".products-carousel-list").owlCarousel({
                        // Most important owl features
                        items: '<?php echo $item ?>',
                        itemsCustom : false,
                        itemsDesktop : [1199,<?php echo $item; ?>],
                        itemsDesktopSmall : [980,<?php if($item>1) { echo $item-1; }else{echo 1;} ?>],
                        itemsTablet: [768,<?php if($item>2) { echo $item-2; }else{echo 1;} ?>],
                        itemsTabletSmall: false,
                        itemsMobile : [479,1],
                        singleItem : false,
                        itemsScaleUp : false,
                        // Navigation
                        pagination: false,
                        navigation: true,
                        navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                        rewindNav: true,
                        scrollPerPage: false
                    })
                });
            </script>
            <?php
        }
        if(isset($atts['element_custom_class']))
        $class .= ' ' . $atts['element_custom_class'];
        $query_shop = new WP_Query(apply_filters('woocommerce_shortcode_products_query', $atts));
        $query_shop->query($atts);
        ?>

        <?php if (isset($atts['title'])&& $atts['title']!='')
            echo '<h3 class="title"><span>' . $atts['title'] . '</span></h3>';
        ?>
        <div class="<?php echo esc_attr($owlCarousel) ?>">
            <ul class="products clearfix <?php echo esc_attr($class) ?>">
                <?php while ($query_shop->have_posts()) {
                    $query_shop->the_post();
                    global $product;
                    ?>
                    <?php
                    if ($atts['products_type'] == 'products_list') {
                        echo rit_get_template_part('woocommerce-item/list', 'item', array('atts' => $atts));
                    } else {
                        echo rit_get_template_part('woocommerce-item/grid', 'item', array('atts' => $atts));
                    }
                    ?>
                    <?php
                }
                ?>
            </ul>
        </div>
    </div>
<?php
wp_reset_postdata();