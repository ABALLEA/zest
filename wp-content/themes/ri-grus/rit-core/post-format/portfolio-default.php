<?php
$class = '';
if (has_post_format('video')):
    ?>
    <div class="wrapper-img">
    <?php $sp_video = get_post_meta(get_the_ID(), '_format_video_embed', true); ?>
    <?php if (wp_oembed_get($sp_video)) : ?>
    <?php echo wp_oembed_get($sp_video); ?>
<?php else : ?>
    <?php echo $sp_video; ?>
<?php endif; ?>
    </div><?php
elseif (has_post_format('audio')) :
    if ($atts['portfolio_layout'] == 'medium') echo '<div class="' . $class . '">' ?>
    <div class="wrapper-img">
        <?php $sp_audio = get_post_meta($post->ID, '_format_audio_embed', true); ?>
        <?php if (wp_oembed_get($sp_audio)) : ?>
            <?php echo wp_oembed_get($sp_audio); ?>
        <?php else : ?>
            <?php echo $sp_audio; ?>
        <?php endif; ?>
    </div>
    <?php
elseif (has_post_format('gallery')) : if ($atts['portfolio_layout'] == 'medium') echo '<div class="' . $class . '">' ?>

    <?php $images = get_post_meta($post->ID, '_format_gallery_images', true); ?>

    <?php if ($images) : ?>
        <div class="wrapper-img">
            <ul class="bxslider">
                <?php foreach ($images as $image) : ?>
                    <?php $the_image = wp_get_attachment_image_src($image, 'full-thumb'); ?>
                    <?php $the_caption = get_post_field('post_excerpt', $image); ?>
                    <li><img src="<?php echo $the_image[0]; ?>"
                             <?php if ($the_caption) : ?>title="<?php echo $the_caption; ?>"<?php endif; ?> />
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <?php
else:
    ?>
    <div class="wrapper-img">
        <?php if ($atts['portfolio_layout'] == 'masonry'):
            ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                <?php the_post_thumbnail('full-thumb'); ?>
            </a>
            <?php
        else:
            if (get_post_meta(get_the_ID(), 'rit_detail_image', true) != '') {
                //if ($atts['portfolio_layout'] == 'medium' || $atts['portfolio_layout'] == 'large' || $atts['portfolio_layout'] == 'full'){
                $thumb_arg = wp_get_attachment_image_src(get_post_meta(get_the_ID(), 'rit_detail_image', true), 'full', true);
                //}
                //else $thumb_arg=wp_get_attachment_image_src(get_post_meta( get_the_ID(), 'rit_detail_image', true ), 'medium', true);
                $thumb_url = $thumb_arg[0];
                ?>
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <img class="thumb-related-portfolio" src="<?php echo $thumb_url ?>" alt="<?php the_title(); ?>"/>
                </a>
            <?php }
        endif;
        ?>
        <?php if (!empty($atts['view_more'])) { ?>
            <div class="wrapper-mask">
                <div class="mask"><span class="readmore"><a href="<?php the_permalink(); ?>"
                                                            title="<?php the_title(); ?>"> <?php echo __('Read more', 'ri-grus') ?> </a> </span>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php
endif; ?>