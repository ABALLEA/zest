<?php
/**
 * The default template for displaying content
 *
 */
get_header();
?>
    <div id="primary" class="content-area page-content row">
        <main class="main-content site-main col-xs-12">
            <?php
            // Start the loop.
            while (have_posts()) : the_post(); ?>
                <article
                    id="post-<?php the_ID(); ?>" <?php (is_single()) ? post_class('news-item single-postfolio') : post_class('post-item news-item single-postfolio'); ?>>

                    <div class="news-info">
                        <?php
                        if (get_post_meta(get_the_ID(), 'rit_detail_image', true) != '') {
                            $thumb_arg = wp_get_attachment_image_src(get_post_meta(get_the_ID(), 'rit_detail_image', true), 'full', true);
                            $thumb_url = $thumb_arg[0];
                            ?>
                            <img class="thumb-postfolio" src="<?php echo esc_url($thumb_url) ?>" alt="<?php the_title(); ?>"/>
                        <?php }
                        ?>
                        <div class="description">
                            <?php
                            if (is_single()) {
                                if (get_post_meta($post->ID, 'rit_portfolio_extend_info_status', true) == "enable"):
                                    ?>
                                    <div class="row">
                                        <h2 style="padding:0 15px; margin-top:50px"><?php the_title(); ?></h2>
                                        <div class="col-xs-12 col-md-6">
                                            <ul class="des-portfolio">
                                                <?php if(get_post_meta($post->ID, 'rit_client_portfolio', true) != '') {?>
                                                <li>
                                                    <span><?php _e("Client",'ri-grus') ?></span><?php echo esc_html(get_post_meta($post->ID, 'rit_client_portfolio', true)); ?>
                                                </li>
                                                <?php }; ?>
                                                <?php if(get_post_meta($post->ID, 'rit_date_complete_portfolio', true) != '') {?>
                                                    <li>
                                                        <span><?php _e("Date complete",'ri-grus') ?></span><?php echo esc_html(get_post_meta($post->ID, 'rit_date_complete_portfolio', true)); ?>
                                                    </li>
                                                <?php }; ?>
                                                <?php if(get_post_meta($post->ID, 'rit_live_demo_portfolio', true) != '') {?>
                                                    <li>
                                                        <span><?php _e("Live demo",'ri-grus') ?></span><?php echo '<a href="'.esc_url(get_post_meta($post->ID, 'rit_live_demo_portfolio', true)).'" title="'.esc_html(get_post_meta($post->ID, 'rit_live_demo_portfolio', true)).'">'.esc_html(get_post_meta($post->ID, 'rit_live_demo_portfolio', true)).'</a>'; ?>
                                                    </li>
                                                <?php }; ?>
                                                <li> <span><?php _e("Categories",'ri-grus') ?></span><?php echo get_the_term_list( $post->ID,'portfolio_category', '', ', ', '' );?></li>
                                                <li class="socail">
                                                    <span><?php _e("Share",'ri-grus') ?></span>
                                                <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" class="post_share_facebook" onclick="javascript:window.open(this.href,
                          '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=220,width=600');return false;"><i class="fa fa-facebook"></i> </a>
                                                <a href="https://twitter.com/share?url=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href,
                          '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=260,width=600');return false;" class="product_share_twitter"><i class="fa fa-twitter"></i></a>
                                                <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href,
                          '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-google-plus"></i></a>
                                                <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php if(function_exists('the_post_thumbnail')) echo wp_get_attachment_url(get_post_thumbnail_id()); ?>&description=<?php echo get_the_title(); ?>"  title="<?php _e('Share to pinterest','ri-grus')?>"><i class="fa fa-pinterest"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <?php if(get_post_meta($post->ID, 'rit_short_des_portfolio', true)!=''){?>
                                        <div class="col-xs-12 col-md-6" style="line-height: 24px;  letter-spacing: 0.5px; padding:10px 0">
                                            <?php echo get_post_meta($post->ID, 'rit_short_des_portfolio', true);?>
                                            </div>
                                    <?php }?>
                                    </div>
                                    <?php
                                endif;
                                the_content();
                            } else {
                                echo rit_content(50);
                            }
                            ?>
                        </div>
                        <!-- .entry-content -->
                    </div>
                </article>
                <!-- #post-## -->
                <div id="control-portfolio" class="clearfix">
                    <div class="prev-portfolio ct-port pull-left">
                        <?php previous_post_link('%link', '<span class="arrow"></span>'); ?>
                    </div>
                    <div class="back-to-home">
                        <a href="<?php echo esc_url(home_url('/')); ?>" rel="home"
                           title="<?php _e('Back to Project', 'ri-grus') ?>"><span></span></a>
                    </div>
                    <div class="next-portfolio ct-port pull-right">
                        <?php next_post_link('%link', '<span class="arrow"></span>'); ?>
                    </div>
                </div>
                <?php
                get_template_part('related', 'portfolio');
            endwhile; ?>
        </main>
    </div><!-- .content-area -->

<?php get_footer(); ?>