<?php
/**
 * Created by PhpStorm.
 * User: NTK
 * Date: 14/07/2015
 * Time: 5:08 CH
* Template Name: Portfolio page
 */
get_header();?>
<div id="primary" class="content-area portfolio-page">
        <main class="site-main">
            <?php
            // Start the loop.
            while ( have_posts() ) : the_post();
                // Include the page content template.
                the_content();
                // End the loop.
            endwhile;
            ?>
        </main><!-- .site-main -->
    </div><!-- .content-area -->
    <?php get_footer(); ?>
