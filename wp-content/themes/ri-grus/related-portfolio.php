<?php
/**
 * Created by PhpStorm.
 * User: NTK
 * Date: 06/07/2015
 * Time: 5:30 CH
 */
$args=array(
    //'tag__in' => $tag_ids,
    'post__not_in' => array($post->ID),
    'showposts'=>3,  // Number of related posts that will be shown.
    'post_type'=> 'portfolio'
);

$the_query = new wp_query($args);
if ($the_query->have_posts()):?>
    <div class="wrapper-related-portfolio">
        <h3 class="title"><?php _e('Related portfolio','ri-grus')?></h3>
        <ul class="row">
<?php
    while ($the_query->have_posts()): $the_query->the_post();?>
<li <?php post_class('col-sm-4 col-xs-12 related-portfolio-item')?>>
    <figure>
        <?php
        if(get_post_meta( get_the_ID(), 'rit_detail_image', true )!=''){
            $thumb_arg=wp_get_attachment_image_src(get_post_meta( get_the_ID(), 'rit_detail_image', true ), 'medium', true);
            $thumb_url=$thumb_arg[0];
            ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                <img class="thumb-related-portfolio" src="<?php echo esc_url($thumb_url) ?>" alt="<?php the_title();?>"/>
            </a>
        <?php }
        ?>
        <figcaption>
            <h3 class="title-news"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
            <?php echo get_the_term_list(get_the_ID(), 'portfolio_category', ' ', ' / ', ' '); ?>
        </figcaption>
    </figure>
</li>
<?php endwhile;
endif;?>
        </ul>
    </div>