<?php
add_action( 'vc_load_default_templates_action','grus_home_template_for_vc' ); // Hook in

function grus_home_template_for_vc() {
    $data               = array(); // Create new array
    $data['name']       = __( ' Grus Home Portfolio', 'ri-grus' ); // Assign name for your custom template
    $data['weight']     = 0; // Weight of your template in the template list
    //$data['image_path'] = preg_replace( '/\s/', '%20', plugins_url( 'images/custom_template_thumbnail.jpg', __FILE__ ) ); // Always use preg replace to be sure that "space" will not break logic. Thumbnail should have this dimensions: 114x154px
    $data['custom_class'] = 'custom_template_for_vc_custom_template'; // CSS class name
    $data['content']    = '[vc_row][vc_column][portfolio portfolio_layout="masonry" view_more="" cat="16,18,17,20,19"][/vc_column][/vc_row]';
    vc_add_default_templates( $data );
}
?>