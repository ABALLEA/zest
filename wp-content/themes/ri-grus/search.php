<?php
/**
 * The template for displaying search results pages.
 *
 * @package WordPress
 * @subpackage Grus
 * @since Grus 1.0
 */
?>
<?php get_header(); ?>
<div id="primary" <?php post_class('content-area row '); ?>>
    <?php if (have_posts()) : ?>
        <main class="site-main search-result-page col-xs-12">


            <header class="page-header">
                <h1 class="page-title"><?php printf(__('Search Results for: %s', "ri-grus"), get_search_query()); ?></h1>
            </header>
            <!-- .page-header -->

            <?php
            // Start the loop.
            while (have_posts()) : the_post(); ?>

                <?php
                /*
                 * Run the loop for the search to output the results.
                 * If you want to overload this in a child theme then include a file
                 * called content-search.php and that will be used instead.
                 */
                get_template_part('content','search');

                // End the loop.
            endwhile;

            // Previous/next page navigation.
            if (function_exists("posts_nav")) :
                posts_nav();
            endif; ?>
        </main>
    <?php // If no content, include the "No posts found" template.
    else :?>
        <main class="site-main search-result-page col-xs-12">
            <?php
            get_template_part('content', 'none'); ?>
        </main>
        <?php
    endif;
    ?>
</div>
<?php get_footer(); ?>
