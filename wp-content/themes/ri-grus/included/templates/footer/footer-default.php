<div id="top-footer" class="clearfix">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <?php dynamic_sidebar('footer-1'); ?>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <?php dynamic_sidebar('footer-2') ?>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <?php dynamic_sidebar('footer-3') ?>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <?php dynamic_sidebar('footer-4') ?>
        </div>
    </div>
</div>
<div id="bottom-footer" class="clearfix">
    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <?php if (get_theme_mod('rit_enable_copyright', '1')) { ?>
                <div class="copy-right" id="copy-right">
                    <?php if (get_theme_mod('rit_copyright_text')) {
                        echo get_theme_mod('rit_copyright_text');
                    }?>
                </div><!-- .site-info -->
            <?php } ?>
        </div>
        <div class="col-sm-6 col-xs-12">
            <?php get_template_part('included/templates/socail'); dynamic_sidebar('bottom-footer') ?>
        </div>
    </div>
</div>