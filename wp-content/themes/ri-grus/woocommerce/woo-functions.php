<?php
/**
 * Created by PhpStorm.
 * User: NTK
 * Date: 6/22/2015
 * Time: 4:18 PM
 * All custom function of woo.
 */
//Custom number product display
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return '.get_theme_mod('rit_number_products_display').';' ), 20);
//Add image for product category page
add_action( 'woocommerce_archive_description', 'woocommerce_category_image', 2 );
function woocommerce_category_image() {
    if ( is_product_category() ){
        global $wp_query;
        $cat = $wp_query->get_queried_object();
        $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
        $image = wp_get_attachment_url( $thumbnail_id );
        if ( $image ) {
            echo '<div class="wrapper-thumb-cat"><img src="' . $image . '" alt="" /></div>';
        }
    }
}
//sidebar
function woo_widget(){
    register_sidebar( array(
        'name'          =>  __( 'Right product sidebar', 'ri-grus' ),
        'id'            => 'pright_sidebar',
        'description'   => __( 'Widget area of product page.', 'ri-grus' ),
        'before_widget' => '<aside id="%1$s" class="sidebar-item %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="title"><span>',
        'after_title'   => '</span></h3>',
    ) );
}
add_action( 'widgets_init', 'woo_widget' );

//Update topcart when addtocart
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );

function woocommerce_header_add_to_cart_fragment( $fragments ) {
    ob_start();
    ?>
    <?php get_template_part('included/templates/topheadcart'); ?>
    <?php
    $fragments['#topcart'] = ob_get_clean();
    return $fragments;
}
//Custom hook for product page woocommerce
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating',5);
add_action( 'woocommerce_template_single_rating', 'woocommerce_template_single_rating', 11 );

add_action('woocommerce_after_shop_loop_item', 'show_add_to_wishlist',10 );
function show_add_to_wishlist()
{
    echo do_shortcode('[yith_wcwl_add_to_wishlist]');
}
/* WISHLIST BUTTON
 ================================================== */
if (!function_exists('ct_wishlist_button')) {
    function ct_wishlist_button($class = "") {

        global $product, $yith_wcwl;

        if ( class_exists( 'YITH_WCWL_UI' ) )  {
            $url = $yith_wcwl->get_wishlist_url();
            $product_type = $product->product_type;
            $exists = $yith_wcwl->is_product_in_wishlist( $product->id );
            $text=__("Wishlist","ri-grus");
            $classes = get_option( 'yith_wcwl_use_button' ) == 'yes' ? 'class="add_to_wishlist single_add_to_wishlist button alt"' : 'class="add_to_wishlist"';

            $html = '<div class="yith-wcwl-add-to-wishlist '.$class.'">';
            $html .= '<div class="yith-wcwl-add-button';  // the class attribute is closed in the next row

            $html .= $exists ? ' hide" style="display:none;"' : ' show"';

            $html .= '><a href="' . htmlspecialchars($yith_wcwl->get_addtowishlist_url()) . '" data-product-id="' . $product->id . '" data-product-type="' . $product_type . '" ' . $classes . ' ><i class="fa fa-heart-o"></i>'.$text.'</a>';
            $html .= '</div>';

            $html .= '<div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;"><span class="feedback">' . __( 'Product added to wishlist.', 'cootheme' ) . '</span> <a href="' . $url . '"><i class="fa fa-check-circle"></i></a></div>';
            $html .= '<div class="yith-wcwl-wishlistexistsbrowse ' . ( $exists ? 'show' : 'hide' ) . '" style="display:' . ( $exists ? 'block' : 'none' ) . '"><a href="' . $url . '"><i class="fa fa-check-circle"></i></a></div>';

            $html .= '</div>';

            return $html;
        }
    }
}
//Custom number products related display
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
function jk_related_products_args( $args ) {
    if(get_theme_mod('rit_number_products_related_display')==''){
        $args['posts_per_page'] = 4; // 4 related products
    }
    else
        $args['posts_per_page'] = get_theme_mod('rit_number_products_related_display');
    //$args['columns'] = 2; // arranged in 2 columns
    return $args;
}
