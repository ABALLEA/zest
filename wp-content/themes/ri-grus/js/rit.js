/**
 * Created by chinhbeo on 5/11/15.
 */


jQuery(document).ready(function(){

    // ---------------------------------------- //
    // SLIDER OWL ----------------------------- //
    // ---------------------------------------- //
    jQuery('#owl-demo').owlCarousel({
        items : 1,
        navigation: true,
        navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
        autoPlay : false,
        pagination: false
    });

    // ---------------------------------------- //
    // SLIDER BX ------------------------------ //
    // ---------------------------------------- //
    jQuery('.bxslider').each(function(){
        jQuery(this).bxSlider({
            pager : false
        });
    });


    // ---------------------------------------- //
    // SCROLLER TOP --------------------------- //
    // ---------------------------------------- //
    jQuery('.arrow-down a').click(function(){
        var element = jQuery(this).attr('href');
        jQuery('html, body').animate({
            scrollTop: jQuery(element).offset().top
        }, 1000);
        return false;
    });


    // ---------------------------------------- //
    // BACK TO TOP --------------------------- //
    // ---------------------------------------- //
    jQuery('#back-to-top').click(function(e){
        e.preventDefault();
        jQuery('html, body').animate({
            scrollTop: 0
        }, 1000);
        return false;
    });

    // ---------------------------------------- //
    // CANVAS MENU ---------------------------- //
    // ---------------------------------------- //
    var menuWrap = jQuery('body').find('.menu-wrap i'),
        mainWrapper = jQuery('body'),
        iconClose = jQuery('.canvas-menu .btn-close'),
        canvasOverlay = jQuery('.canvas-overlay');

    // Function Canvas Menu
    function menuCanvas(){
        if(mainWrapper.hasClass('canvas-open')){
            mainWrapper.removeClass('canvas-open');
        } else {
            mainWrapper.addClass('canvas-open');
        }
    }

    // Call Function Canvas
    menuWrap.click(function(){
        menuCanvas();
    });

    // Click icon close
    iconClose.click(function(){
        menuCanvas();
    });

    // Click canvas
    canvasOverlay.click(function(){
        menuCanvas();
    });

    // ---------------------------------------- //
    // ACCORDION MENU ------------------------- //
    // ---------------------------------------- //
    var iconAcc = '<span class="menu-arrow fa fa-plus"></span>',
        subMenu = jQuery('#main-navigation').find('.sub-menu');

    // Append Arrow Menu
    jQuery(subMenu).closest('.menu-item').append(iconAcc);

    // Toggle Menu
    jQuery('#main-navigation .menu-arrow').click(function(){
        jQuery(this).closest('ul').find('.sub-menu').slideUp();
        jQuery(this).prev().slideDown();
        jQuery('#main-navigation .menu-arrow').removeClass('fa-minus').addClass('fa-plus');
        if((jQuery(this).prev()).is(":visible")){
            jQuery(this).removeClass('fa-plus').addClass('fa-minus');
        } else {
            jQuery(this).removeClass('fa-minus').addClass('fa-plus');
        }
    });
});