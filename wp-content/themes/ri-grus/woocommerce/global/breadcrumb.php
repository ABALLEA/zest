<?php
/**
 * Shop breadcrumb
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 * @see         woocommerce_breadcrumb()
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $breadcrumb ) {

   
     if ( is_shop() ){
     		 echo('<ul class="col-xs-12 col-sm-8 breadcrumbs breadcrumbs_shop_page pull-right">');
     }
	else{
		echo('<ul class="col-xs-12 col-sm-8 breadcrumbs pull-right">');
		echo '<li><a href="' . get_home_url() . '">ACCUEIL</a></li><li>&nbsp/&nbsp</li>';
		echo '<li><a href="' . get_home_url() . '/shop/">SHOP</a></li>';
	}
	
	foreach ( $breadcrumb as $key => $crumb ) {

		echo $before;


		if ( ! empty( $crumb[1] ) && sizeof( $breadcrumb ) !== $key + 1 ) {
			echo '<li class="woocommerce_breadcrumb_'.esc_html( $crumb[0]).' "><a href="' . esc_url( $crumb[1] ) . '">' . esc_html( $crumb[0] ) . '</a></li>';
		} else {
			echo '<li><strong>'.esc_html($crumb[0]).'</strong></li>' ;
		}

		echo $after;

		if ( sizeof( $breadcrumb ) !== $key + 1 ) {
			echo '<li>'.$delimiter.'</li>';
		}

	}
echo('</ul>');

}