<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header();
global $post;

$sidebar = $class_main = $class_content = $class_sidebar = '';
if(get_post_meta($post->ID, 'rit_sidebar_options', true) == 'use-default'){
    $sidebar = get_theme_mod('rit_default_sidebar', 'no-sidebar');
} else {
    $sidebar = get_post_meta($post->ID, 'rit_sidebar_options', true);
}
$class_content = $sidebar;
if(get_post_type($post->ID)=='portfolio'){
    $class_main = 'col-sm-12 col-md-12';
}
else{
    if($sidebar == 'no-sidebar' || $sidebar ==''){
        $class_main = 'col-sm-12 col-md-12';
    } elseif ($sidebar == 'left-sidebar' || $sidebar == 'right-sidebar'){
        $class_main = 'col-sm-12 col-md-9 ';
    } else{
        $class_main = 'col-sm-12 col-md-6 ';
    }
}
if (is_single()){
    $class_main .='body-content';
}
?>

<div id="primary" class="content-area page-content row <?php echo esc_attr($class_content); ?>">
    <?php if($sidebar == 'left-sidebar' || $sidebar == 'both-sidebar') { ?>
        <div id="sidebar-left" class="col-sm-12 col-md-3">
            <?php
            if(get_post_meta($post->ID, 'rit_sidebar_options', true) == 'use-default'){

                $sidebar_left = get_theme_mod('rit_left_sidebar', 'sidebar-1');
            } else {
                $sidebar_left = get_post_meta($post->ID, 'rit_left_sidebar', true);
            }
            dynamic_sidebar( $sidebar_left );
            ?>
        </div>
    <?php } ?>
    <main class="main-content site-main <?php echo esc_attr($class_main); ?>">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			/*
			 * Include the post format-specific template for the content. If you want to
			 * use this in a child theme, then include a file called called content-___.php
			 * (where ___ is the post format) and that will be used instead.
			 */
			get_template_part( 'content', get_post_format() );

			// If comments are open or we have at least one comment, load up the comment template.
            if (is_single()):
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;
            endif;

		// End the loop.
		endwhile;
		?>

    </main><!-- .site-main -->
    <?php if($sidebar == 'right-sidebar' || $sidebar == 'both-sidebar') { ?>
        <div id="sidebar-right" class="col-sm-12 col-md-3">
            <?php
            if(get_post_meta($post->ID, 'rit_sidebar_options', true) == 'use-default' || get_post_meta($post->ID, 'rit_sidebar_options', true)==''){

                $sidebar_right = get_theme_mod('rit_right_sidebar', 'sidebar-1');
            } else {
                $sidebar_right = get_post_meta($post->ID, 'rit_right_sidebar', true);
            }
            dynamic_sidebar( $sidebar_right );
            ?>
        </div>
    <?php } ?>
</div><!-- .content-area -->
<?php get_footer(); ?>
