<?php
/**
 * Created by PhpStorm.
 * User: ntk
 * Date: 11/06/2015
 * Time: 15:48
 * Template Name:Ma Product page
 */
get_header();
?>
    <div class="woo-category-page">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post();
                        the_content();
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>