<?php
/**
 * The default template for displaying content
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php (is_single()) ? post_class('single-post col-xs-12 col-sm-6 col-md-4  portfolio ') : post_class('post-item col-xs-12 col-sm-6 col-md-4  portfolio '); ?>>

    <?php if(has_post_format('gallery')) : ?>

        <?php $images = get_post_meta( $post->ID, '_format_gallery_images', true ); ?>

        <?php if($images) : ?>
            <div class="post-image<?php echo (is_single()) ? ' single-image' : ''; ?>">
                <ul class="bxslider">
                    <?php foreach($images as $image) : ?>

                        <?php $the_image = wp_get_attachment_image_src( $image, 'full-thumb' ); ?>
                        <?php $the_caption = get_post_field('post_excerpt', $image); ?>
                        <li><img src="<?php echo esc_url($the_image[0]); ?>" <?php if($the_caption) : ?>title="<?php echo esc_attr($the_caption); ?>"<?php endif; ?> /></li>

                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>

    <?php elseif(has_post_format('video')) : ?>

        <div class="post-image<?php echo esc_attr((is_single()) ? ' single-video' : ''); ?>">
            <?php $sp_video = get_post_meta( $post->ID, '_format_video_embed', true ); ?>
            <?php if(wp_oembed_get( $sp_video )) : ?>
                <?php echo esc_html(wp_oembed_get($sp_video)); ?>
            <?php else : ?>
                <?php echo esc_html(($sp_video)); ?>
            <?php endif; ?>
        </div>

    <?php elseif(has_post_format('audio')) : ?>

        <div class="post-image audio<?php echo esc_attr((is_single()) ? ' single-audio' : ''); ?>">
            <?php $sp_audio = get_post_meta( $post->ID, '_format_audio_embed', true ); ?>
            <?php if(wp_oembed_get( $sp_audio )) : ?>
                <?php echo esc_html(wp_oembed_get($sp_audio)); ?>
            <?php else : ?>
                <?php echo esc_html($sp_audio); ?>
            <?php endif; ?>
        </div>

    <?php else : ?>

        <?php if(has_post_thumbnail()) : ?>
            <?php if(!get_theme_mod('sp_post_thumb')) : ?>
                <div class="wrapper-img <?php echo (is_single()) ? ' single-image' : ''; ?>">
                    <a href="<?php echo get_permalink() ?>" title="<?php the_title();?>">
                        <?php
                        if(get_post_meta( get_the_ID(), 'rit_detail_image', true )!=''):
                            $thumb_arg=wp_get_attachment_image_src(get_post_meta( get_the_ID(), 'rit_detail_image', true ), 'full', true);
                            $thumb_url=$thumb_arg[0];
                            ?>
                            <img class="thumb-postfolio" src="<?php echo esc_url($thumb_url) ?>" alt="<?php the_title();?>"/>
                        <?php else:
                            the_post_thumbnail('thumbnail');
                           endif;
                            ?>
                    </a>
                    <?php if (!is_single()): ?>
                        <div class="wrapper-mask">
                            <div class="mask" data-link="<?php the_permalink()?>">
                <span class="readmore">
                    <a href="<?php echo get_permalink() ?>" title="<?php echo __('Read more', 'ri-grus' )?>"><?php echo __('Read more', 'ri-grus' )?></a>
                </span>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>

    <?php endif; ?>
    <div class="news-info">
        <?php if(get_theme_mod('rit_enable_page_heading', '1')) { ?>
            <?php
            if (is_single()) :
                the_title('<h3 class="title-news">', '</h3>');
            else :
                the_title(sprintf('<h3 class="title-news"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h3>');
            endif;
            ?>
        <?php } ?>
        <p class="info-post">
            <?php echo get_the_term_list($post->ID, 'portfolio_category', ' ', ' / ', ' '); ?></p>
    </div>
</article><!-- #post-## -->
