<?php
/**
 * Created by PhpStorm.
 * User: ntk
 * Date: 08/06/2015
 * Time: 15:26
 */
$args = array(
    'post_type' => 'portfolio',
    'posts_per_page' => ($atts['number'] > 0) ? $atts['number'] : get_option('posts_per_page')
);

if ($atts['cat']) {
    $cat = explode(',', $atts['cat']);
    $args['tax_query'] = array(
        array(
            'taxonomy' => 'portfolio_category',
            'field' => 'id',
            'terms' => $cat,
        )
    );
}

if ($atts['post_in'])
    $args['post__in'] = explode(',', $atts['post_in']);
$args['paged'] = (get_query_var('paged')) ? get_query_var('paged') : 1;

$the_query=new WP_Query($args);
if($the_query->have_posts()):?>
<div class="rit-full-layout">
    <?php
    while($the_query->have_posts()):$the_query->the_post();
        //get list catslug
        $catslug = '';
        $termspost = get_the_terms( get_the_ID(), 'portfolio_category');
        if ($termspost && !is_wp_error($termspost)) :
            foreach ($termspost as $term) :
                $catslug .= $term->slug . ' ';
            endforeach;
        endif;
        ?>
        <article class="rit-news-item <?php echo esc_attr($catslug) ?>" id="portfolio-<?php the_ID(); ?>">
            <?php echo rit_get_template_part('post-format/portfolio', 'default', array('atts' => $atts)) ?>
            <div class="rit-news-info">
                <h3 class="title-portfolio"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                <p class="info-post"><a class="author-link" href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>" rel="author">
                        <?php echo(get_the_author()); ?>
                    </a> / <?php echo get_the_date('F jS, Y'); ?>
                    /  <?php comments_number('0 Comment', '1 Comment', '% Comments');?> </p>
                <?php echo get_the_term_list( $post->ID, '', '/', '' ); ?>
            </div>
        </article>
<?php
    endwhile;?>
</div>
    <?php
    if($atts['pagination'] == 'standard') :
        if (function_exists("rit_pagination")) :
            rit_pagination(3, $the_query);
        endif;
    endif;
endif;
wp_reset_postdata();