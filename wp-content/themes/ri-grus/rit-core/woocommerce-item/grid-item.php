<?php
/**
 * Created by PhpStorm.
 * User: NTK
 * Date: 10/08/2015
 * Time: 11:38 SA
 */
$class = '';
if ($atts['products_type'] != 'products_carousel') {
    switch ($atts['column']) {
        case 'columns1':
            $class = 'col-xs-12';
            break;
        case 'columns2':
            $class = 'col-xs-12 col-sm-6';
            break;
        case 'columns3':
            $class = 'col-xs-12 col-sm-4';
            break;
        case 'columns4':
            $class = 'col-xs-12 col-sm-3';
            break;
        case 'columns5':
            $class = 'col-xs-12 col-sm-1-5';
            break;
        case 'columns6':
            $class = 'col-xs-12 col-sm-2';
            break;
        default:
            $class = 'col-xs-12 col-sm-4';
            break;
    }
}
global $post, $product;
?>
<li class="<?php echo esc_attr(implode(' ', get_post_class($class))) ?>">
    <div class="wrapper-product">
        <div class="woo-wrapper-thumb">
            <?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">

                <?php if ($product->is_on_sale()) : ?>

                    <?php echo apply_filters('woocommerce_sale_flash', '<span class="onsale">' . __('Sale!', 'woocommerce') . '</span>', $post, $product); ?>

                <?php endif; ?>
                <?php
                echo wp_get_attachment_image(get_post_thumbnail_id($post->ID), $atts['products_img_size']);
                ?>
            </a>
        </div>
        <div class="wrapper-product-info">
            <div class="woo-product-button">
                <div class="group-grid-button">
                    <?php

                    /**
                     * woocommerce_after_shop_loop_item hook
                     *
                     * @hooked woocommerce_template_loop_add_to_cart - 10
                     */
                    do_action('woocommerce_after_shop_loop_item');

                    ?>
                </div>
            </div>
            <div class="woo-product-info">
                <h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                <?php
                /**
                 * woocommerce_after_shop_loop_item_title hook
                 *
                 * @hooked woocommerce_template_loop_rating - 5
                 * @hooked woocommerce_template_loop_price - 10
                 */
                do_action( 'woocommerce_after_shop_loop_item_title' );
                ?>
                <?php do_action('woocommerce_template_single_rating');?>
            </div>
        </div>
    </div>

</li>