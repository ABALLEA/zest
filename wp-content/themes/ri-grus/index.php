<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Grus
 * @since Grus 1.0
 */

get_header();
global $post;

$sidebar = $class_main = $class_content = $class_sidebar = '';
$sidebar = get_theme_mod('rit_default_sidebar', 'right-sidebar');
$class_content = $sidebar;

if($sidebar == 'no-sidebar' || $sidebar ==''){
    $class_main = 'col-sm-12 col-md-12';
} elseif ($sidebar == 'left-sidebar' || $sidebar == 'right-sidebar'){
    $class_main = 'col-sm-12 col-md-9 ';
} else{
    $class_main = 'col-sm-12 col-md-6 ';
}

?>

<div id="primary" class="content-area row <?php echo esc_attr($class_content); ?>">
    <?php if($sidebar == 'left-sidebar' || $sidebar == 'both-sidebar') { ?>
        <div id="sidebar-left" class="sidebar col-sm-12 col-md-3">
			<?php dynamic_sidebar(get_theme_mod('rit_left_sidebar','sidebar-1')); ?>
        </div>
    <?php } ?>
		<main class="site-main <?php echo $class_main; ?>">

		<?php if ( have_posts() ) : ?>

			<?php if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
			<?php endif; ?>

			<?php

			// Start the loop.
            $i=0;
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				if(get_theme_mod('rit_default_blog_layout')=='large'){
					get_template_part( 'content','large');
				}
				else
					get_template_part( 'content');

			// End the loop.
                $i++;
			endwhile;

			// Previous/next page navigation.
            //if (function_exists("pagination")) :
             //   pagination(get_option('posts_per_page'));
            //endif;
			if (function_exists("posts_nav")) :
				posts_nav();
			endif;
		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>

		</main><!-- .site-main -->
    <?php if($sidebar == 'right-sidebar' || $sidebar == 'both-sidebar') { ?>
        <div id="sidebar-right" class="sidebar col-sm-12 col-md-3">
			<?php dynamic_sidebar(get_theme_mod('rit_right_sidebar','sidebar-1')); ?>
        </div>
    <?php } ?>
</div><!-- .content-area -->

<?php get_footer(); ?>
