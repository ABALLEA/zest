<?php

/**
 * The sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Grus
 * @since Grus 1.0
 */


global $post;
$sidebar_right = get_theme_mod('rit_default_right_sidebar', 'sidebar-1');

?>

<?php if ( is_active_sidebar( $sidebar_right ) ) : ?>
    <div id="widget-area" class="widget-area" role="complementary">
        <?php dynamic_sidebar( $sidebar_right ); ?>
    </div><!-- .widget-area -->
<?php endif; ?>
