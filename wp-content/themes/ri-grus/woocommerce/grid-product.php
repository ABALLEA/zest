<?php
/**
 * Created by PhpStorm.
 * User: NTK
 * Date: 16/07/2015
 * Time: 11:16 SA
 */
$classes[]="col-xs-12 col-sm-4 col-md-3";
?>
<li <?php post_class( $classes ); ?>>
<div class="wrapper-product">
    <div class="woo-wrapper-thumb">
        <?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">

            <?php
            /**
             * woocommerce_before_shop_loop_item_title hook
             *
             * @hooked woocommerce_show_product_loop_sale_flash - 10
             * @hooked woocommerce_template_loop_product_thumbnail - 10
             */
            do_action( 'woocommerce_before_shop_loop_item_title' );
            ?>
        </a>
    </div>
    <div class="wrapper-product-info">
        <div class="woo-product-button">
            <div class="group-grid-button">
                <?php

                /**
                 * woocommerce_after_shop_loop_item hook
                 *
                 * @hooked woocommerce_template_loop_add_to_cart - 10
                 */
                do_action('woocommerce_after_shop_loop_item');

                ?>
                
            </div>
            <div class="voir_article">
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <span>VOIR L'ARTICLE</span>
                </a>
            </div>
            
        </div>
        <div class="woo-product-info">
            <h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
            <?php
            /**
             * woocommerce_after_shop_loop_item_title hook
             *
             * @hooked woocommerce_template_loop_rating - 5
             * @hooked woocommerce_template_loop_price - 10
             */
            do_action( 'woocommerce_after_shop_loop_item_title' );
            ?>
            <?php do_action('woocommerce_template_single_rating');?>
        </div>
    </div>
</div>

</li>