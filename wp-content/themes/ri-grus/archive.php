
<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Grus
 * @since  1.0
 */

get_header();
global $post;

$sidebar = $class_main = $class_content = $class_sidebar = '';

$sidebar = get_theme_mod('rit_default_sidebar', 'no-sidebar');
$class_content = $sidebar;

if($sidebar == 'no-sidebar' || $sidebar ==''){
    $class_main = 'col-sm-12 col-md-12';
} elseif ($sidebar == 'left-sidebar' || $sidebar == 'right-sidebar'){
    $class_main = 'col-sm-12 col-md-9';
} else{
    $class_main = 'col-sm-12 col-md-6';
}

?>
    <div id="primary" class="archive-page content-area row <?php echo esc_attr($class_content); ?>">
        <?php if($sidebar == 'left-sidebar' || $sidebar == 'both-sidebar') { ?>
            <div id="sidebar-left" class="col-sm-12 col-md-3">
                <?php dynamic_sidebar(get_theme_mod('rit_left_sidebar','sidebar-1')); ?>
            </div>
        <?php } ?>
		<main class="site-main <?php echo esc_attr($class_main); ?>">
		<?php if ( have_posts() ) : ?>

			<?php
			// Start the Loop.
            if(get_post_type($post->ID)=='portfolio'){
                echo '<div class="row wrapper-list-portfolio">';
            }
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
                if(get_post_type($post->ID)=='portfolio'){
                    get_template_part('portfolio', 'page');
                }
                else {
                    if(get_theme_mod('rit_default_blog_layout')=='large'){
                        get_template_part( 'content','large');
                    }
                    else
                        get_template_part( 'content');
                }

			// End the loop.
			endwhile;
            if(get_post_type($post->ID)=='portfolio') {
                echo '</div>';
            }
            if (function_exists("posts_nav")) :
                posts_nav();
            endif;
			// Previous/next page navigation.
            /*if (function_exists("pagination")) :
                pagination(get_option('posts_per_page'));
            endif;*/

		// If no content, include the "No posts found" template.
		else:
			get_template_part( 'content', 'none' );
		endif;
		?>

		</main><!-- .site-main -->
        <?php if($sidebar == 'right-sidebar' || $sidebar == 'both-sidebar') { ?>
        <div id="sidebar-right" class="col-sm-12 col-md-3">
            <?php dynamic_sidebar(get_theme_mod('rit_right_sidebar','sidebar-1')); ?>
        </div>
        <?php } ?>
    </div><!-- .content-area -->
<?php get_footer(); ?>
