<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *<?php wp_nav_menu( array( 'theme_location' => 'menu-produits', 'container_class' => 'menu-produits' ) ); ?>
 *<?php wp_nav_menu( array( 'theme_location' => 'menu-connexion', 'container_class' => 'menu-connexion' ) ); ?>
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

get_header(); ?>
<?php if (have_posts()) : ?>

    <div class="wrapper-breadcrumb woo-breadcrumb breadcrumb-portfolio-page">
        <div class="container">
            <div class="row">
                <?php if (apply_filters('woocommerce_show_page_title', true)) : ?>
                    <h2 class="page-title col-xs-12 col-sm-4"><?php woocommerce_page_title(); ?></h2>
                <?php endif;
                /**
                 * woocommerce_before_main_content hook
                 *
                 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                 * @hooked woocommerce_breadcrumb - 20
                 */
                do_action('woocommerce_before_main_content');
                ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (is_shop()):?>
<div class="container woo-category-page shop-page">
<?php else: ?>
<div class="container woo-category-page">
<?php endif;?>
    <div class="row">
       <div id="right-sidebar" class="col-xs-12 col-sm-12 col-md-3">
            <?php dynamic_sidebar('pright_sidebar') ?>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-9">
            <?php if (have_posts()) : ?>
                <div class="header-woopagecat">
                    <div class="woo-opt-page row">
                       
                        <div class="ordergroup col-xs-12 col-sm-9">
                            <?php
                            /**
                             * woocommerce_before_shop_loop hook
                             *
                             * @hooked woocommerce_result_count - 20
                             * @hooked woocommerce_catalog_ordering - 30
                             */
                            do_action('woocommerce_before_shop_loop');
                            ?>
                        </div>
                        <div class="pageview pull-right col-xs-12 col-sm-3">
                            <?php _e('View as: ', 'ri-grus');
                            if (!isset($_COOKIE['product-view'])): ?>
                
                                <span data-view="grid" class="pageviewitem <?php
                                if (get_theme_mod('rit_product_layout_view') == 'grid'):
                                    echo esc_attr('active');
                                endif; ?>"><i class="fa fa-th"></i> </span>
                                <span data-view="list" class="pageviewitem  <?php
                                if (get_theme_mod('rit_product_layout_view') != 'grid'):
                                    echo esc_attr('active');
                                endif; ?>"><i class="fa fa-list"></i> </span>
                                <?php
                            else:
                                ?>
                                <span data-view="grid" class="pageviewitem <?php
                                if ($_COOKIE['product-view'] == 'grid'):
                                    echo esc_attr('active');
                                endif; ?>"><i class="fa fa-th"></i> </span>
                                <span data-view="list" class="pageviewitem  <?php
                                if ($_COOKIE['product-view'] != 'grid'):
                                    echo esc_attr('active');
                                endif; ?>"><i class="fa fa-list"></i> </span>
                                <?php
                            endif; ?>
                        </div>
                    </div>
                </div>
                <?php woocommerce_product_loop_start(); ?>
                
<!--                <?php woocommerce_product_subcategories(); ?>-->
                           <?php if ( is_shop() ){
                $image_wasted = get_field( 'image_fond_wasted', 484); 
                $image_tealer = get_field( 'image_fond_tealer', 484); 
                $image_zest = get_field( 'image_fond_zest', 484); 
                $image_newBrand = get_field( 'image_fond_new_brand', 484); 
                if( !empty($image_wasted) && !empty($image_tealer) && !empty($image_zest) && !empty($image_newBrand) ): ?>


                        <div class="mainCat">
                            <div class="colxs-12 col-sm-6 col-md-3"><a style="background-image:url(<?php echo $image_wasted['url']; ?>" href="http://dev.zestore:8888/categorie-produit/vetements/?filter_vetement-brand=wasted&query_type_vetement-brand=or"><span>WASTED</span></a></div>
                            <div class="colxs-12 col-sm-6 col-md-3"><a style="background-image:url(<?php echo $image_tealer['url']; ?>" href="./categorie-produit/vetements/?filter_vetement-brand=tealer"><span>TEALER</span></a></div>
                            <div class="colxs-12 col-sm-6 col-md-3"><a style="background-image:url(<?php echo $image_zest['url']; ?>" href="categorie-produit/vetements/?filter_vetement-brand=zest&query_type_vetement-brand=or"><span>ZEST</span></a></div>
                            <div class="colxs-12 col-sm-6 col-md-3"><a style="background-image:url(<?php echo $image_newBrand['url']; ?>" href="./categorie-produit/vetements/?filter_vetement-brand=octobtre-noir%2Charrington%2Cflyzer&query_type_vetement-brand=or"><span>NEW BRAND</span></a></div>
                        </div>
                   <?php endif; 

             
                
              
                    echo(' <div class="col-xs-12 produits_recents">
                    <h2 class="alignCenter">NEWS</h2>
            
                    
                </div>');
                 }
                ?>

               



                <?php while (have_posts()) : the_post();
                    wc_get_template_part('content', 'product');
                endwhile; // end of the loop. ?>

                <?php woocommerce_product_loop_end(); ?>
            
                <?php
                /**
                 * woocommerce_after_shop_loop hook
                 *
                 * @hooked woocommerce_pagination - 10
                 */
                do_action('woocommerce_after_shop_loop');
                ?>
            <?php elseif (!woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) : ?>
                <div class="row no-product">
                    <?php wc_get_template('loop/no-products-found.php'); ?>
                </div>
                
            <?php endif; ?>


            <?php
            /**
             * woocommerce_after_main_content hook
             *
             * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
             */
            //do_action( 'woocommerce_after_main_content' );
            ?>
        </div>
        <?php
        /**
         * woocommerce_sidebar hook
         *
         * @hooked woocommerce_get_sidebar - 10
         */
        //do_action( 'woocommerce_sidebar' );
        ?>
        
        
            <?php do_action('woocommerce_archive_description'); ?>
    
    </div>
</div>

<?php get_footer(); ?>
