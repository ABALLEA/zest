<?php
/**
 * Created by PhpStorm.
 * User: ntk
 * Date: 08/06/2015
 * Time: 16:21
 */
$args = array(
    'post_type' => 'portfolio',
    'posts_per_page' => ($atts['number'] > 0) ? $atts['number'] : get_option('posts_per_page')
);
if ($atts['cat'] != '') {
    $cat = explode(',', $atts['cat']);
    $args['tax_query'] = array(
        array(
            'taxonomy' => 'portfolio_category',
            'field' => 'id',
            'terms' => $cat
        )
    );
}
if ($atts['post_in'] != '') {
    $args['post__in'] = explode(',', $atts['post_in']);
}
$args['paged'] = (get_query_var('paged')) ? get_query_var('paged') : 1;
$start_timeline = 0;
 $the_query = new WP_Query($args);
    if ($the_query->have_posts()):?><div id="portfolio-timeline-layout" class="rit-timeline-layout"><?php
        while ($the_query->have_posts()):$the_query->the_post();
            global $prev_post_year, $prev_post_month, $post_count;

            $post_month = get_the_date('n');
            $post_year = get_the_date('o');
            $current_date = get_the_date('o-n');
            ?>
            <?php if ($prev_post_month != $post_month || ($prev_post_month == $post_month && $prev_post_year != $post_year) || $start_timeline == 0) : ?>
                <?php
                $post_count = 1;
                $start_timeline = 1;
                ?>
                <div class="clear"></div>
                <div class="timeline-date"><h3><?php echo get_the_date('F Y'); ?></h3></div>
            <?php endif;
            $classes = 'timeline-box ';
            $classes .= ($post_count % 2 == 1 ? 'left' : 'right');
            ?>

            <article class="rit-news-item  col-xs-12 col-sm-6 <?php echo esc_attr($classes) ?>" id="post-<?php the_ID(); ?>">
                <div class="rit-item-masonry">
                    <?php echo rit_get_template_part('post-format/portfolio', 'default', array('atts' => $atts)) ?>
                    <div class="rit-masonry-mask">
                        <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"></a>

                        <div class="rit-wrapper-mask">
                            <h3 class="rit-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                            <h4 class="rit-cat"><?php echo get_the_term_list(get_the_ID(), 'portfolio_category', ' ', ' / ', ' '); ?></h4>
                        </div>

                    </div>
                </div>
            </article>
            <?php
            $prev_post_year = $post_year;
            $prev_post_month = $post_month;
            $post_count++;
        endwhile;?></div><?php
        if($atts['pagination'] == 'standard') :
            if (function_exists("rit_pagination")) :
                rit_pagination(3, $the_query);
            endif;
        endif;
    endif;
    wp_reset_postdata();