<?php
/**
 * The template for displaying comments
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package WordPress
 * @subpackage Grus
 * @since Grus 1.0
 */
$commenter = wp_get_current_commenter();
$req = get_option( 'require_name_email' );
$aria_req = ( $req ? " aria-required='true'" : '' );
/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
    return;
}
?>
<div id="comments" class="comments-area">
    <div class="row">
        <div id="total-comments" class="col-sm-6 col-xs-12"><?php comments_number( __('0 Comment','ri-grus'), __('1 Comment','ri-grus'),__('% Comment','ri-grus') ); ?></div>
        <div id="write-comments" class="col-sm-6 col-xs-12"><a href="#commentform"><i class="fa fa-pencil-square-o"> </i><?php _e('Leave a comment now','ri-grus')?></a></div>
    </div>
    <?php if ( have_comments() ) : ?>
        <ul class="list-comments">
            <?php
            wp_list_comments( array(
                'style'       => 'ul',
                'short_ping'  => true,
                'avatar_size' => 80,
                'callback'=>'mytheme_comment',
            ) );
            ?>
        </ul><!-- .comment-list -->
        <?php
        echo '<div class="pager" id="comment-pagination">';
            paginate_comments_links( array('prev_text' => '<i class="fa fa-chevron-left"></i>', 'next_text' => '<i class="fa fa-chevron-right"></i>') );
        echo '</div>';
    endif; // have_comments() ?>
    <?php
    // If comments are closed and there are comments, let's leave a little note, shall we?
    if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
        ?>
        <p class="no-comments"><?php _e( 'Comments are closed.', 'ri-grus' ); ?></p>
    <?php endif; ?>
    <?php $comment_args = array( 'title_reply'=>__('Leave a comment','ri-grus'),
        'comment_notes_before' =>'<div class="row"><div class="col-xs-12 col-md-4">',
        'logged_in_as'=>'<div class="row"><div class="col-xs-12 col-md-4">'
            . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>','ri-grus' ),
                admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ),
        'fields' => apply_filters( 'comment_form_default_fields', array(

            'author' => '<p>'.

                '<input id="author"  class="ipt text" placeholder="Your Name" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /><i class="fa fa-user"></i></p>',

            'email'  => '<p>' .
                '<input id="email"  class="ipt text" name="email" placeholder="Email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /><i class="fa fa-envelope"></i></p>',

            'url'    => '' ) ),

        'comment_field' => '</div><div class="col-xs-12 col-md-8">' .

            '<textarea id="comment" class="textarea text" name="comment" cols="45" rows="8" aria-required="true"></textarea>' .

            '</div>',
        'class_submit'=>'btn btn-submit',
        'comment_notes_after' => '</div>',

    );
    comment_form($comment_args); ?>
</div><!-- .comments-area -->
