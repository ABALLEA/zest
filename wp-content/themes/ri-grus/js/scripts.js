jQuery(document).ready(function () {
    jQuery('a:has(img)').addClass('afternotvisible');
    //Active MobileNav
    MobileNav();
    resizeVideo();
//Header js
    if (jQuery('#wpadminbar').outerHeight() > 0) {
        jQuery('.header-page').css('top', jQuery('#wpadminbar').outerHeight());
    }
    var h = parseInt(jQuery('.header-page').outerHeight());
    //Call function toTop with input height of topnav(header-page)
    if (jQuery(document).scrollTop() > h / 2) {
        jQuery('.header-page').not('.static-header').addClass('visible-header');
    }
    var check = 0;
    //Effect for topnav when window scroll
    jQuery(window).bind("scroll", function () {
        if (jQuery(document).scrollTop() > 100) {
            jQuery('.header-page').not('.static-header').addClass('visible-header');
        }
        if (jQuery(document).scrollTop() < 100 && 0 <= jQuery(document).scrollTop()) {
            jQuery('.header-page').removeClass('visible-header');
        }
    });

//Search js
    jQuery('#searchbtnhead').click(function () {
        jQuery(this).toggleClass('active');
        jQuery("#global-search").toggleClass('active');
        jQuery("#global-search .ipt").focus();
    });
    jQuery('#closesearch').click(function () {
        jQuery("#global-search").toggleClass('active');
    })
    //Add icon for slider
    jQuery('.da-arrows-prev').append('<i class="fa fa-angle-left"></i>');
    jQuery('.da-arrows-next').append('<i class="fa fa-angle-right"></i>')

    //jQuery('.widget_product_categories .cat-parent').prepend('<span class="showsubnav triggersubnav"><i class="fa fa-plus"></i> </span>');
    //toggleNav('.triggersubnav', '.children');

    //jQuery('.sidebar-item ul li:has(ul)').prepend('<span class="showsubnav triggersubnav"><i class="fa fa-plus"></i> </span>');
   // toggleNav('.triggersubnav', '.children');
    //Selectic js
    jQuery('select').selectric();

    //Woo js
    setTimeout(function () {
        jQuery('.woocommerce-message').css('right', '0');
    }, 500);
    setTimeout(function () {
        jQuery('.woocommerce-message').removeAttr('style');
    }, 3000);
    //Set cookie for change product layout
    jQuery('.pageviewitem').not('.active').click(function () {
        if (jQuery.cookie('product-view') != '') {
            jQuery.cookie('product-view', jQuery(this).attr('data-view'));
            document.location.reload();
        }
    });
    //Add redirect of click event for grid view products
    jQuery('.grid-layout .mask-link').click(function () {
        if(jQuery(this).attr('data-link')!=undefined){
				window.location.href = jQuery(this).attr('data-link');
			}
    })
    jQuery('.wrapper-mask .mask').click(
        function () {
			if(jQuery(this).attr('data-link')!=undefined){
				window.location.href = jQuery(this).attr('data-link');
			}
			else{
				console.log('und')
			}
        }
    )
    jQuery('.woocommerce-pagination ul.page-numbers').removeClass('page-numbers');
});

//Mobile Nav Js control
function MobileNav() {
    // if (jQuery(window).width() <= 768) {
    jQuery('body').append('<div id="mask-mobile-nav"></div>');
    var navmobile = jQuery(".wrapper-top-nav").clone();
    jQuery('body').prepend(navmobile);
    jQuery('body>.wrapper-top-nav').addClass('navmobile');
    jQuery('body>.wrapper-top-nav').prepend('<div class="control-mobile-nav"><span class="pull-right"></span></div>');
    jQuery('#mask-mobile-nav').click(function () {
        jQuery(this).removeClass('active');
        jQuery('.mobile-nav').removeClass('active');
        jQuery('.navmobile').removeClass('active');
        jQuery('.wrapper-page').removeClass('active');
    });
    jQuery('.control-mobile-nav, .mobile-nav').click(function () {
        jQuery('#mask-mobile-nav').toggleClass('active');
        jQuery('.mobile-nav').toggleClass('active');
        jQuery('.navmobile').toggleClass('active');
        jQuery('.wrapper-page').toggleClass('active');
    });
    jQuery('.navmobile .menu-item-has-children').append('<span class="showsubnav triggersubnav"><i class="fa fa-angle-right"></i> </span>');
    toggleNav('.triggersubnav', '.sub-menu');
    jQuery('.nav-menu .triggersubnav').click(function () {
        if (jQuery(this).hasClass('active')) {
            jQuery(this).prev('.sub-menu').find('.active').removeClass('active');
        }
        jQuery(this).toggleClass('active');
    });
    //}
}
//function control navitem show and hide.
function toggleNav(trigger, target) {

    var h;
    jQuery(trigger).click(function () {
        //change icon
        if (jQuery(this).children('i').hasClass('fa-plus')) {
            jQuery(this).children('i').removeClass('fa-plus').addClass('fa-minus');
        }
        else {
            jQuery(this).children('i').removeClass('fa-minus').addClass('fa-plus');
        }
        //end change icon
        jQuery.this = jQuery(this).parent().children(target);
        if (jQuery.this.height() == 0) {
            //get height of first child
            h = 0;
            for (var i = jQuery.this.children('li').length; i > 0; i--) {
                h = h + jQuery.this.children('li:nth-child(' + i + ')').outerHeight();
            }
            //Add height for animation
            jQuery.this.css('height', h + 'px');
            jQuery.this.parents(target).css('height', h + jQuery.this.parents(target).outerHeight() + 'px');
        } else {
            h = jQuery.this.outerHeight();
            jQuery.this.css('height', 0 + 'px');
            jQuery.this.parents(target).css('height', jQuery.this.parents(target).outerHeight() - h + 'px');
        }
        h = 0;
    });
}
//Scroll to top
//Back to top
jQuery(window).scroll(function () {
    if (jQuery(this).scrollTop() > 100) {
        jQuery('#back-to-top').addClass('show');
    } else {
        jQuery('#back-to-top').removeClass('show');
    }
});
jQuery('#back-to-top').click(function () {
    jQuery('body,html').animate({
        scrollTop: 0
    }, 800);
    return false;
});
window.onload = function (){
    setTimeout(function(){
        if(jQuery('.sticky-wrapper').length){
            jQuery('.forcefullwidth_wrapper_tp_banner').addClass('hassticky');
        }
    },1000);

}
//=====================Resize video=====================//
function resizeVideo(){
    // Find all YouTube videos
    var allVideos = jQuery(".post-image iframe");

// Figure out and save aspect ratio for each video
    allVideos.each(function() {
        jQuery(this).data('aspectRatio', this.height / this.width)
            // and remove the hard coded width/height
            .removeAttr('height')
            .removeAttr('width');

    });

// When the window is resized
    jQuery(window).resize(function() {

        var newWidth =  jQuery(".post-image:has(iframe)").width();
        // Resize all videos according to their own aspect ratio
        allVideos.each(function() {

            var el = jQuery(this);
            el
                .width(newWidth)
                .height(newWidth * el.data('aspectRatio'));

        });
// Kick off one resize to fix all videos on page load
    }).resize();
}