<?php
/**
 * Created by PhpStorm.
 * User: chinhbeo
 * Date: 5/11/15
 * Time: 10:40 AM
 */

?>

<div class="slider-wrap">
    <div class="arrow-down">
        <a href="#content"><i class="fa fa-angle-down"></i></a>
    </div>
    <?php if(is_home()) { ?>
    <div id="owl-demo" class="owl-carousel">

<!--        --><?php
//
//        $featured_cat = get_theme_mod( 'sp_featured_cat' );
//        $number = get_theme_mod( 'sp_featured_slider_slides' );
//
//        ?>

        <?php $feat_query = new WP_Query( array( 'cat' => 4, 'showposts' => 10 ) ); ?>
        <?php if ($feat_query->have_posts()) : while ($feat_query->have_posts()) : $feat_query->the_post(); ?>
            <div class="item">
                <div class="feat-image"><a href="<?php echo get_permalink() ?>"><?php the_post_thumbnail('full'); ?></a></div>
                <div class="feat-overlay">
                    <div class="feat-text">
                        <span class="feat-cat"><?php the_category(', '); ?></span>
                        <h3 class="feat-title"><a href="<?php echo get_permalink() ?>"><?php echo rit_substring(get_the_title(), 10); ?></a></h3>
                        <a class="feat-link" href="<?php echo get_permalink() ?>"><?php echo __('EXPLORE', 'rit'); ?></a>
                    </div>
                </div>
            </div>
        <?php endwhile; endif; ?>

    </div>
    <?php } else { ?>
        <div class="poster-image">
            <?php
                global $post;
                $poster_image = get_post_meta($post->ID, 'rit_poster_image', true);
                $disible_title = get_post_meta($post->ID, 'rit_disible_title', true);
                $custom_title = get_post_meta($post->ID, 'rit_custom_title', true);
                $sub_title = get_post_meta($post->ID, 'rit_sub_title', true);
                if($poster_image != ''){
                    echo wp_get_attachment_image($poster_image, 'full');
                } else {
                    echo '<img src="'. get_template_directory_uri() .'/images/poster-image.jpg" alt="Poster Image" />';
                }
            ?>

            <?php if(!$disible_title) { ?>
                <div class="custom-title">
                    <?php if(is_single() || is_page()) { ?>
                    <h1><?php echo ($custom_title) ? $custom_title : the_title(); ?></h1>
                    <?php } else { ?>
                        <h1><?php echo single_cat_title( '', false ); ?></h1>
                    <?php } ?>
                    <?php if($sub_title) { ?>
                    <span class="sub-title"><?php echo $sub_title; ?></span>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>