<?php
/**
 * Created by PhpStorm.
 * User: NTK
 * Date: 6/25/2015
 * Time: 9:41 AM
 */ ?>
<div id="topcart">
    <?php
    global $woocommerce;

    // get cart quantity
    $qty = $woocommerce->cart->get_cart_contents_count();

    $cart_url = $woocommerce->cart->get_cart_url();
    ?>
    <a href="<?php echo $cart_url ?>" title="<?php _e('Cart', 'ri-grus') ?>">
        <span data-icon="&#xe013;"></span>
    </a>
    <span class="total-product"><?php echo esc_html($qty) ?></span>

    <div class="wrapper-totalcart">
        <div class="content-cart">
            <div class="widget_shopping_cart_content"> <?php woocommerce_mini_cart(); ?></div>
        </div>
    </div>
</div>