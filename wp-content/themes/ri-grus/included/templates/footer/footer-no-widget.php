<?php
/**
 * Created by PhpStorm.
 * User: NTK
 * Date: 29/07/2015
 * Time: 8:50 SA
 */
?>
<div id="bottom-footer" class="clearfix no-widget">
    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <?php if (get_theme_mod('rit_enable_copyright', '1')) { ?>
                <div class="copy-right" id="copy-right">
                    <?php if (get_theme_mod('rit_copyright_text')) {
                        echo get_theme_mod('rit_copyright_text');
                    }?>
                </div><!-- .site-info -->
            <?php } ?>
        </div>
        <div class="col-sm-6 col-xs-12">
            <?php get_template_part('included/templates/socail'); dynamic_sidebar('bottom-footer') ?>
        </div>
    </div>
</div>