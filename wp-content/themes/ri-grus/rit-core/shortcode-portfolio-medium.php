<?php
/**
 * Created by PhpStorm.
 * User: ntk
 * Date: 08/06/2015
 * Time: 13:59
 */
$args = array(
    'post_type' => 'portfolio',
    'posts_per_page' => ($atts['number'] > 0) ? $atts['number'] : get_option('posts_per_page')
);

if ($atts['cat']) {
    $cat = explode(',', $atts['cat']);
    $args['tax_query'] = array(
        array(
            'taxonomy' => 'portfolio_category',
            'field' => 'id',
            'terms' => $cat,
        )
    );
}
$class='';

switch ($atts['columns']) {
    case '2':
        $class= 'col-sm-6 col-xs-12';
        break;
    case '3':
        $class= 'col-sm-4 col-xs-12';
        break;
    case '4':
        $class= 'col-sm-3 col-xs-12';
        break;
    case '5':
        $class= 'col-sm-3 col-xs-12';
        break;
    case '6':
        $class= 'col-sm-2 col-xs-12';
        break;
    default:
        $class= 'col-xs-12';
}
if ($atts['post_in'])
    $args['post__in'] = explode(',', $atts['post_in']);
$args['paged'] = (get_query_var('paged')) ? get_query_var('paged') : 1;
?>
    <?php echo rit_get_template_part('post-format/nav','portfolio', array('atts' => $atts)) ?>

<?php
$the_query=new WP_Query($args);

if($the_query->have_posts()):?><div id="wrapper-rit-item-masonry" class="rit-medium-layout"><?php
    while($the_query->have_posts()): $the_query->the_post();
        //get list catslug
        $termspost = get_the_terms( get_the_ID(), 'portfolio_category');
        if ($termspost && !is_wp_error($termspost)) :
            foreach ($termspost as $term) :
                $class .=  ' '.$term->slug;
            endforeach;
        endif;
        ?>
        <article class="rit-news-item all rit-item-masonry <?php echo  esc_attr($class) ?>" id="post-<?php the_ID(); ?>">
            <?php echo rit_get_template_part('post-format/portfolio', 'default', array('atts' => $atts)) ?>
            <div class="rit-news-info">
                <h3 class="title-portfolio"><a href="<?php the_permalink(); ?>"
                                          title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                <div class="description"><?php if(function_exists('rit_excerpt')){ echo rit_excerpt(12);} ?></div>
            </div>
        </article>
<?php
    endwhile;?></div><?php
    if($atts['pagination'] == 'standard') :
        if (function_exists("rit_pagination")) :
            rit_pagination(3, $the_query);
        endif;
    endif;
    endif;
wp_reset_postdata();