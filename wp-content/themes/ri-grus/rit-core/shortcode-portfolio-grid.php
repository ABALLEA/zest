<?php
/**
 * Created by PhpStorm.
 * User: ntk
 * Date: 08/06/2015
 * Time: 15:09
 */
$args = array(
    'post_type' => 'portfolio',
    'posts_per_page' => ($atts['number'] > 0) ? $atts['number'] : get_option('posts_per_page')
);
if ($atts['cat'] != '') {
    $cat = explode(',', $atts['cat']);
    $args['tax_query'] = array(
        array(
            'taxonomy' => 'portfolio_category',
            'field' => 'id',
            'terms' => $cat
        )
    );
}
if ($atts['post_in'] != '') {
    $args['post__in'] = explode(',', $atts['post_in']);
}
$args['paged'] = (get_query_var('paged')) ? get_query_var('paged') : 1;
$class = '';
switch ($atts['columns']) {
    case '1':
        $class .= " col-xs-12 ";
        break;
    case '2':
        $class .= " col-xs-12 col-sm-6 col-md-6 ";
        break;
    case '3':
        $class .= " col-xs-12 col-sm-6 col-md-4 ";
        break;
    case '4':
        $class .= " col-xs-12 col-sm-6 col-md-3 ";
        break;
}
$the_query = new WP_Query($args);
?>
<?php echo rit_get_template_part('post-format/nav','portfolio', array('atts' => $atts)) ?>
<?php if ($the_query->have_posts()): ?>
    <div id="wrapper-rit-item-masonry" class="rit-grid-layout">
    <?php
    while ($the_query->have_posts()): $the_query->the_post();
        //get list catslug
        $termspost = get_the_terms( get_the_ID(), 'portfolio_category');
        $catslug='';
        $catslug.=$class.' ';
        if ($termspost && !is_wp_error($termspost)) :
            foreach ($termspost as $term) :
                $catslug .=  $term->slug.' ';
            endforeach;
        endif;
        ?>
        <article class="rit-news-item all rit-item-masonry <?php echo esc_attr($catslug); ?>" id="post-<?php the_ID(); ?>">
            <?php echo rit_get_template_part('post-format/portfolio', 'default', array('atts' => $atts)) ?>
            <div class="rit-masonry-mask">
                <a href="<?php the_permalink()?>"></a>
                <div class="rit-wrapper-mask">
                    <h3 class="rit-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <h4 class="rit-cat"><?php echo get_the_term_list(get_the_ID(), 'portfolio_category', ' ', ' / ', ' '); ?></h4>
                </div>

            </div>
        </article>

    <?php endwhile; ?>
    </div><?php
    if($atts['pagination'] == 'standard') :
        if (function_exists("rit_pagination")) :
            rit_pagination(3, $the_query);
        endif;
    endif;
endif;
wp_reset_postdata(); ?>
