<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>
<div class="container single-product">
<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="row">
    <div class="col-xs-12 col-sm-8 col-lg-7">
	<?php
		/**
		 * woocommerce_before_single_product_summary hook
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>
    </div>
	<div class="summary entry-summary col-xs-12 col-sm-4 col-lg-5">

		<?php
			/**
			 * woocommerce_single_product_summary hook
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */
			do_action( 'woocommerce_single_product_summary' );
            //echo do_shortcode( '[yith_wcwl_add_to_wishlist icon="fa-heart-o"]' );
		?>

	</div><!-- .summary -->
    <script>
        jQuery(document).ready(function(){
         jQuery('.cart div.quantity').append('<span class="up"><i class="fa fa-plus"> </span>');
         jQuery('.cart div.quantity').prepend('<span class="down"><i class="fa fa-minus"> </span>');
            jQuery('.cart div.quantity span').click(function(){
                var check= jQuery(this).attr('class');
                var val=parseInt(jQuery('.quantity .qty').val());
                if(check=='up'){
                    jQuery('.quantity .qty').val(val+1);
                }
                else{
                    if(val>1){
                        jQuery('.quantity .qty').val(val-1);
                    }
                }
            });
        });
    </script>
    <div class="col-xs-12 wrapper-detail-product-info">
	<?php
		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>
    </div>
    <?php
        if(get_theme_mod('rit_enable_slider_related_product')=='yes'):?>
            <script type="text/javascript">
                jQuery(document).ready(function () {
            jQuery(".list-related").owlCarousel({
                // Most important owl features
                items :'<?php echo get_theme_mod('rit_number_products_related_display_per_row');?>',
                itemsCustom : false,
                itemsDesktop : [1199,<?php echo get_theme_mod('rit_number_products_related_display_per_row');?>],
                itemsDesktopSmall : [980,<?php if( get_theme_mod('rit_number_products_related_display_per_row')>1) echo  get_theme_mod('rit_number_products_related_display_per_row')-1;else echo  get_theme_mod('rit_number_products_related_display_per_row');?>],
                itemsTablet: [768,<?php if( get_theme_mod('rit_number_products_related_display_per_row')>2) echo  get_theme_mod('rit_number_products_related_display_per_row')-2; else echo  get_theme_mod('rit_number_products_related_display_per_row');?>],
                itemsTabletSmall: false,
                itemsMobile : [479,1],
                singleItem : false,
                itemsScaleUp : false,
                // Navigation
                pagination : false,
                navigation : true,
                navigationText : [" "," "],
                rewindNav : true,
                scrollPerPage : false});})
            </script>
            <?php
    endif;
    ?>
	<meta itemprop="url" content="<?php the_permalink(); ?>" />
</div>
</div><!-- #product-<?php the_ID(); ?> -->
</div>
<?php do_action( 'woocommerce_after_single_product' ); ?>
