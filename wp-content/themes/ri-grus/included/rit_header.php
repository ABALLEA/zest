<?php

// -------------------------------------- //
// CANVAS MENU //
// -------------------------------------- //
if (!function_exists('RIT_MAIN_MENU')) {
    function RIT_MAIN_MENU()
    {
        $html = '';
        $html .= '<div id="main-navigation">';
        $html .= wp_nav_menu(array('theme_location' => 'primary', 'menu_class' => 'nav-menu', 'menu_id' => 'primary-menu'));
        $html .= '</div>';
        


        return $html;
    }
}

// -------------------------------------- //
// MAIN MENU //
// -------------------------------------- //
if (!function_exists('RIT_CANVAS')) {
    function RIT_CANVAS()
    {
        $html = '';
        $html .= '<div class="canvas-menu"><div class="canvas-menu-inner"><span class="btn-close"><i class="fa fa-close"></i></span>'. RIT_MAIN_MENU() .'</div></div>';;


        return $html;
    }
}

// -------------------------------------- //
// LOGO //
// -------------------------------------- //
if (!function_exists('RIT_LOGO')) {
    function RIT_LOGO()
    {
        $html = '';

        if (is_front_page() && is_home()) {
            if (!get_theme_mod('rit_logo')) {
                $html .= '<h1 class="site-title"><a href="' . esc_url(home_url('/')) . '" rel="home">' . bloginfo('name') . '</a></h1>';
            } else {
                $html .= '<h1 class="site-logo" id="logo"><a href="' . esc_url(home_url('/')) . '" rel="home"><img src="' . get_theme_mod('rit_logo') . '" alt="' . bloginfo('name') . '" /></a></h1>';
                if (get_theme_mod('rit_logo_retina')) {
                    $html .= '<h1 class="site-logo" id="logo-retina"><a href="' . esc_url(home_url('/')) . '" rel="home"><img src="' . get_theme_mod('rit_logo_retina') . '" alt="' . bloginfo('name') . '" /></a></h1>';
                }
            }
        } else {
            if (!get_theme_mod('rit_logo')) {
                $html .= '<p class="site-title"><a href="' . esc_url(home_url('/')) . '" rel="home">' . bloginfo('name') . '</a></p>';
            } else {
                $html .= '<p class="site-logo" id="logo"><a href="' . esc_url(home_url('/')) . '" rel="home"><img src="' . get_theme_mod('rit_logo') . '" alt="' . bloginfo('name') . '" /></a></p>';
                if (get_theme_mod('rit_logo_retina')) {
                    $html .= '<p class="site-logo" id="logo-retina"><a href="' . esc_url(home_url('/')) . '" rel="home"><img src="' . get_theme_mod('rit_logo_retina') . '" alt="' . bloginfo('name') . '" /></a></p>';
                }
            }
        }

        return $html;
    }
}
?>