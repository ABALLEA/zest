<?php
/**
 *
 *
 * 'title' => '',
 * 'layout' => 'grid',
 * 'columns' => '4',
 * 'view' => '',
 * 'cat' => '',
 * 'post_in' => '',
 * 'number' => 8,
 * 'view_more' => false,
 * 'animation_type' => '',
 * 'animation_duration' => '',
 * 'animation_delay' => '',
 * 'el_class' => ''
 *
 */
$args = array(
    'post_type' => 'portfolio',
    'posts_per_page' => ($atts['number'] > 0) ? $atts['number'] : get_option('posts_per_page')
);

if ($atts['cat']) {
    $cat = explode(',', $atts['cat']);
    $args['tax_query'] = array(
        array(
            'taxonomy' => 'portfolio_category',
            'field' => 'id',
            'terms' => $cat,
        )
    );
}

if ($atts['post_in'])
    $args['post__in'] = explode(',', $atts['post_in']);
$args['paged'] = (get_query_var('paged')) ? get_query_var('paged') : 1;

$the_query = new WP_Query($args); ?>
<?php echo rit_get_template_part('post-format/nav','portfolio', array('atts' => $atts)) ?>
<?php
if ($the_query->have_posts()) :?>
    <div id="wrapper-rit-item-masonry" class="rit-large-layout"><?php
    while ($the_query->have_posts()) : $the_query->the_post();
        //get list catslug
        $class='';
        $termspost = get_the_terms( get_the_ID(), 'portfolio_category');
        if ($termspost && !is_wp_error($termspost)) :
            foreach ($termspost as $term) :
                $class .=  ' '.$term->slug;
            endforeach;
        endif;
        ?>
        ?>
        <article class="rit-news-item rit-item-masonry <?php echo esc_attr($class) ?>" id="portfolio-<?php the_ID(); ?>">
            <?php rit_get_template_part('post-format/portfolio', 'default', array('atts' => $atts)) ?>
            <div class="rit-news-info">
                <h3 class="title-portfolio"><a href="<?php the_permalink(); ?>"
                                          title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>

                <p class="info-post"><a class="author-link"
                                        href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"
                                        rel="author">
                        <?php echo(get_the_author()); ?>
                    </a> / <?php echo get_the_date('F jS, Y'); ?>
                    / <?php comments_number('0 Comment', '1 Comment', '% Comments'); ?>
                    / <?php echo get_the_term_list(get_the_ID(), 'portfolio_category', '<i class="fa fa-tags"></i> ', ', ', ''); ?>
                </p>

                <div class="description"><?php the_excerpt(); ?></div>
            </div>
        </article>
        <?php
    endwhile; ?>
    </div><?php
    if($atts['pagination'] == 'standard') :
        if (function_exists("rit_pagination")) :
            rit_pagination(3, $the_query);
        endif;
    endif;
endif;
wp_reset_postdata();